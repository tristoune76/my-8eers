<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210607220906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE recipe (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, biere_id INT DEFAULT NULL, houblons LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', grains LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', levures LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', additifs LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', state INT DEFAULT NULL, comments LONGTEXT DEFAULT NULL, palier LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', vol_h2_oempatage DOUBLE PRECISION DEFAULT NULL, vol_h2_orincage DOUBLE PRECISION DEFAULT NULL, vol_houblonnage DOUBLE PRECISION DEFAULT NULL, vol_empatage DOUBLE PRECISION DEFAULT NULL, vol_final_obtenu DOUBLE PRECISION DEFAULT NULL, vol_fermentation DOUBLE PRECISION DEFAULT NULL, d_initiale DOUBLE PRECISION DEFAULT NULL, f_finale DOUBLE PRECISION DEFAULT NULL, d_rincage DOUBLE PRECISION DEFAULT NULL, INDEX IDX_DA88B137A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recipe_template (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recipe ADD CONSTRAINT FK_DA88B137A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE recipe');
        $this->addSql('DROP TABLE recipe_template');
    }
}

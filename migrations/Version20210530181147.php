<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210530181147 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stock_register (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, type_ingredient VARCHAR(255) NOT NULL, quantite DOUBLE PRECISION DEFAULT NULL, aromes VARCHAR(255) DEFAULT NULL, ebcmin DOUBLE PRECISION DEFAULT NULL, ebcmax DOUBLE PRECISION DEFAULT NULL, acides_alpha_min DOUBLE PRECISION DEFAULT NULL, acides_alpha_max DOUBLE PRECISION DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, stock_id INT NOT NULL, date_log DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE stock_register');
    }
}

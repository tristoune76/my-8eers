<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531190608 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock_register ADD raison LONGTEXT DEFAULT NULL, DROP nom, DROP type_ingredient, DROP aromes, DROP ebcmin, DROP ebcmax, DROP acides_alpha_min, DROP acides_alpha_max, DROP type, CHANGE quantite quantite DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock_register ADD nom VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD type_ingredient VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD aromes VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD ebcmin DOUBLE PRECISION DEFAULT NULL, ADD ebcmax DOUBLE PRECISION DEFAULT NULL, ADD acides_alpha_min DOUBLE PRECISION DEFAULT NULL, ADD acides_alpha_max DOUBLE PRECISION DEFAULT NULL, ADD type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP raison, CHANGE quantite quantite DOUBLE PRECISION DEFAULT NULL');
    }
}

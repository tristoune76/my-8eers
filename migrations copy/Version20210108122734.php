<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210108122734 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient ADD biere_id INT NOT NULL, ADD stock_id INT NOT NULL');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870A71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_6BAF7870A71147CC ON ingredient (biere_id)');
        $this->addSql('CREATE INDEX IDX_6BAF7870DCD6110 ON ingredient (stock_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870A71147CC');
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870DCD6110');
        $this->addSql('DROP INDEX IDX_6BAF7870A71147CC ON ingredient');
        $this->addSql('DROP INDEX IDX_6BAF7870DCD6110 ON ingredient');
        $this->addSql('ALTER TABLE ingredient DROP biere_id, DROP stock_id');
    }
}

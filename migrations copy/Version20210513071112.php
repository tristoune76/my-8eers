<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210513071112 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock_register ADD date_ajout DATE NOT NULL');
        $this->addSql('ALTER TABLE stock_register ADD CONSTRAINT FK_103F1456A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_103F1456A76ED395 ON stock_register (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock_register DROP FOREIGN KEY FK_103F1456A76ED395');
        $this->addSql('DROP INDEX IDX_103F1456A76ED395 ON stock_register');
        $this->addSql('ALTER TABLE stock_register DROP date_ajout');
    }
}

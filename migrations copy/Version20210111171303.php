<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210111171303 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ebullition ADD biere_id INT NOT NULL');
        $this->addSql('ALTER TABLE ebullition ADD CONSTRAINT FK_E57914BA71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('CREATE INDEX IDX_E57914BA71147CC ON ebullition (biere_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ebullition DROP FOREIGN KEY FK_E57914BA71147CC');
        $this->addSql('DROP INDEX IDX_E57914BA71147CC ON ebullition');
        $this->addSql('ALTER TABLE ebullition DROP biere_id');
    }
}

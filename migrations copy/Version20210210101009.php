<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210210101009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE brassage (id INT AUTO_INCREMENT NOT NULL, biere_id INT NOT NULL, vol_final_prevu DOUBLE PRECISION DEFAULT NULL, palier LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', tps_empattage DOUBLE PRECISION DEFAULT NULL, vol_h2_oempatage DOUBLE PRECISION DEFAULT NULL, vol_h2_orincage DOUBLE PRECISION DEFAULT NULL, vol_houblonnage DOUBLE PRECISION DEFAULT NULL, tps_houblonnage INT DEFAULT NULL, d_initiale INT DEFAULT NULL, vol_fermentation DOUBLE PRECISION DEFAULT NULL, tps_fermentation INT DEFAULT NULL, d_finale INT DEFAULT NULL, sucre INT DEFAULT NULL, vol_final_obtenu DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_D63A399FA71147CC (biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brassage ADD CONSTRAINT FK_D63A399FA71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE brassage');
    }
}

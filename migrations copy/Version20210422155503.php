<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210422155503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE brassage (id INT AUTO_INCREMENT NOT NULL, biere_id INT NOT NULL, vol_final_prevu DOUBLE PRECISION DEFAULT NULL, tps_empatage DOUBLE PRECISION DEFAULT NULL, vol_h2_oempatage DOUBLE PRECISION DEFAULT NULL, vol_h2_orincage DOUBLE PRECISION DEFAULT NULL, vol_houblonnage DOUBLE PRECISION DEFAULT NULL, tps_houblonnage INT DEFAULT NULL, d_initiale INT DEFAULT NULL, vol_fermentation DOUBLE PRECISION DEFAULT NULL, tps_fermentation INT DEFAULT NULL, d_finale INT DEFAULT NULL, sucre INT DEFAULT NULL, vol_final_obtenu DOUBLE PRECISION DEFAULT NULL, state VARCHAR(255) NOT NULL, transition VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D63A399FA71147CC (biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE etape_brassage (id INT AUTO_INCREMENT NOT NULL, etat INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE palier (id INT AUTO_INCREMENT NOT NULL, biere_id INT NOT NULL, temps INT NOT NULL, temperature DOUBLE PRECISION NOT NULL, INDEX IDX_8EC5C579A71147CC (biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parametre_brassage (id INT AUTO_INCREMENT NOT NULL, etape_brassage_id INT NOT NULL, nom VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, label_link_etape VARCHAR(255) DEFAULT NULL, unite VARCHAR(255) DEFAULT NULL, helper_etape VARCHAR(255) DEFAULT NULL, commentaires LONGTEXT DEFAULT NULL, INDEX IDX_79F7AFDA3A6BEB10 (etape_brassage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE brassage ADD CONSTRAINT FK_D63A399FA71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE palier ADD CONSTRAINT FK_8EC5C579A71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE parametre_brassage ADD CONSTRAINT FK_79F7AFDA3A6BEB10 FOREIGN KEY (etape_brassage_id) REFERENCES etape_brassage (id)');
        $this->addSql('DROP TABLE catalogue');
        $this->addSql('DROP TABLE ebullition');
        $this->addSql('DROP TABLE empatage');
        $this->addSql('DROP TABLE ing_recette');
        $this->addSql('ALTER TABLE biere ADD date_brassage DATE DEFAULT NULL, ADD date_brassee DATE DEFAULT NULL, ADD current_place VARCHAR(255) DEFAULT NULL, ADD vol_final_prevu DOUBLE PRECISION DEFAULT NULL, ADD tps_empatage DOUBLE PRECISION DEFAULT NULL, ADD vol_h2_oempatage DOUBLE PRECISION DEFAULT NULL, ADD vol_h2_orincage DOUBLE PRECISION DEFAULT NULL, ADD vol_houblonnage DOUBLE PRECISION DEFAULT NULL, ADD tps_houblonnage INT DEFAULT NULL, ADD d_initiale INT DEFAULT NULL, ADD vol_fermentation DOUBLE PRECISION DEFAULT NULL, ADD tps_fermentation INT DEFAULT NULL, ADD d_finale INT DEFAULT NULL, ADD sucre INT DEFAULT NULL, ADD vol_final_obtenu DOUBLE PRECISION DEFAULT NULL, ADD transition VARCHAR(255) DEFAULT NULL, ADD poids_grain DOUBLE PRECISION DEFAULT NULL, ADD vol_empatage DOUBLE PRECISION DEFAULT NULL, DROP volume_final, DROP volume_prevu, DROP ingredient_state, CHANGE state state VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870A71147CC');
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870E3D257B9');
        $this->addSql('DROP INDEX IDX_6BAF7870A71147CC ON ingredient');
        $this->addSql('DROP INDEX IDX_6BAF7870E3D257B9 ON ingredient');
        $this->addSql('ALTER TABLE ingredient ADD type_ingredient VARCHAR(255) NOT NULL, ADD is_brewed TINYINT(1) DEFAULT NULL, DROP type_ingredient_id, CHANGE stock_id stock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B365660E3D257B9');
        $this->addSql('DROP INDEX IDX_4B365660E3D257B9 ON stock');
        $this->addSql('ALTER TABLE stock ADD type_ingredient VARCHAR(255) NOT NULL, ADD description LONGTEXT DEFAULT NULL, DROP type_ingredient_id');
        $this->addSql('ALTER TABLE type_ingredient ADD description LONGTEXT DEFAULT NULL, ADD header VARCHAR(255) DEFAULT NULL, ADD ingredient_view_field LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', ADD ingredient_field JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parametre_brassage DROP FOREIGN KEY FK_79F7AFDA3A6BEB10');
        $this->addSql('CREATE TABLE catalogue (id INT AUTO_INCREMENT NOT NULL, type_ingredient_id INT NOT NULL, INDEX IDX_59A699F5E3D257B9 (type_ingredient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE ebullition (id INT AUTO_INCREMENT NOT NULL, biere_id INT NOT NULL, temps INT NOT NULL, INDEX IDX_E57914BA71147CC (biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE empatage (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, num_palier INT NOT NULL, temperature DOUBLE PRECISION NOT NULL, temps INT NOT NULL, INDEX IDX_214F1C4F7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE ing_recette (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, ingredient_type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, id_ingredient INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, INDEX IDX_EFE39C7BF7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE catalogue ADD CONSTRAINT FK_59A699F5E3D257B9 FOREIGN KEY (type_ingredient_id) REFERENCES type_ingredient (id)');
        $this->addSql('ALTER TABLE ebullition ADD CONSTRAINT FK_E57914BA71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE empatage ADD CONSTRAINT FK_214F1C4F7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE ing_recette ADD CONSTRAINT FK_EFE39C7BF7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('DROP TABLE brassage');
        $this->addSql('DROP TABLE etape_brassage');
        $this->addSql('DROP TABLE palier');
        $this->addSql('DROP TABLE parametre_brassage');
        $this->addSql('ALTER TABLE biere ADD volume_final DOUBLE PRECISION DEFAULT NULL, ADD volume_prevu DOUBLE PRECISION DEFAULT NULL, ADD ingredient_state TINYINT(1) NOT NULL, DROP date_brassage, DROP date_brassee, DROP current_place, DROP vol_final_prevu, DROP tps_empatage, DROP vol_h2_oempatage, DROP vol_h2_orincage, DROP vol_houblonnage, DROP tps_houblonnage, DROP d_initiale, DROP vol_fermentation, DROP tps_fermentation, DROP d_finale, DROP sucre, DROP vol_final_obtenu, DROP transition, DROP poids_grain, DROP vol_empatage, CHANGE state state TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ingredient ADD type_ingredient_id INT NOT NULL, DROP type_ingredient, DROP is_brewed, CHANGE stock_id stock_id INT NOT NULL');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870A71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870E3D257B9 FOREIGN KEY (type_ingredient_id) REFERENCES type_ingredient (id)');
        $this->addSql('CREATE INDEX IDX_6BAF7870A71147CC ON ingredient (biere_id)');
        $this->addSql('CREATE INDEX IDX_6BAF7870E3D257B9 ON ingredient (type_ingredient_id)');
        $this->addSql('ALTER TABLE stock ADD type_ingredient_id INT NOT NULL, DROP type_ingredient, DROP description');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660E3D257B9 FOREIGN KEY (type_ingredient_id) REFERENCES type_ingredient (id)');
        $this->addSql('CREATE INDEX IDX_4B365660E3D257B9 ON stock (type_ingredient_id)');
        $this->addSql('ALTER TABLE type_ingredient DROP description, DROP header, DROP ingredient_view_field, DROP ingredient_field');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210111171019 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ebullition DROP FOREIGN KEY FK_E57914BF7CAAB59');
        $this->addSql('DROP INDEX IDX_E57914BF7CAAB59 ON ebullition');
        $this->addSql('ALTER TABLE ebullition DROP id_biere_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ebullition ADD id_biere_id INT NOT NULL');
        $this->addSql('ALTER TABLE ebullition ADD CONSTRAINT FK_E57914BF7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('CREATE INDEX IDX_E57914BF7CAAB59 ON ebullition (id_biere_id)');
    }
}

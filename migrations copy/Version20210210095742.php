<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210210095742 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE catalogue');
        $this->addSql('DROP TABLE ebullition');
        $this->addSql('DROP TABLE empatage');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE catalogue (id INT AUTO_INCREMENT NOT NULL, type_ingredient_id INT NOT NULL, INDEX IDX_59A699F5E3D257B9 (type_ingredient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE ebullition (id INT AUTO_INCREMENT NOT NULL, biere_id INT NOT NULL, temps INT NOT NULL, INDEX IDX_E57914BA71147CC (biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE empatage (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, num_palier INT NOT NULL, temperature DOUBLE PRECISION NOT NULL, temps INT NOT NULL, INDEX IDX_214F1C4F7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE catalogue ADD CONSTRAINT FK_59A699F5E3D257B9 FOREIGN KEY (type_ingredient_id) REFERENCES type_ingredient (id)');
        $this->addSql('ALTER TABLE ebullition ADD CONSTRAINT FK_E57914BA71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE empatage ADD CONSTRAINT FK_214F1C4F7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
    }
}

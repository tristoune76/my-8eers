<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210216223028 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere CHANGE state state VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE brassage ADD transition VARCHAR(255) NOT NULL, CHANGE tps_empattage tps_empatage DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere CHANGE state state TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE brassage DROP transition, CHANGE tps_empatage tps_empattage DOUBLE PRECISION DEFAULT NULL');
    }
}

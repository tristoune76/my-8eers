<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210104143752 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ingredients (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, id_type_of_ingredient INT NOT NULL, id_ingredient INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, INDEX IDX_4B60114FF7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ingredients ADD CONSTRAINT FK_4B60114FF7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('DROP TABLE ing_recette');
        $this->addSql('DROP TABLE ingredients_pour_recette');
        $this->addSql('ALTER TABLE ebullition ADD id_type_of_ingredient INT NOT NULL, DROP ingredient_type');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ing_recette (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, id_ingredient INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, ingredient_type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_EFE39C7BF7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE ingredients_pour_recette (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, id_type_of_ingredient INT NOT NULL, id_ingredient INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, INDEX IDX_6BC230A5F7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE ing_recette ADD CONSTRAINT FK_EFE39C7BF7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE ingredients_pour_recette ADD CONSTRAINT FK_6BC230A5F7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('DROP TABLE ingredients');
        $this->addSql('ALTER TABLE ebullition ADD ingredient_type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP id_type_of_ingredient');
    }
}

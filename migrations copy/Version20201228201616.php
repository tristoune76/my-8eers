<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228201616 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ebullition (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, ingredient_type VARCHAR(255) NOT NULL, id_ingredient INT NOT NULL, temps INT NOT NULL, INDEX IDX_E57914BF7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empatage (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, num_palier INT NOT NULL, temperature DOUBLE PRECISION NOT NULL, temps INT NOT NULL, INDEX IDX_214F1C4F7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ingRecette (id INT AUTO_INCREMENT NOT NULL, id_biere_id INT NOT NULL, ingredient_type VARCHAR(255) NOT NULL, id_ingredient INT NOT NULL, quantite DOUBLE PRECISION NOT NULL, INDEX IDX_EFE39C7BF7CAAB59 (id_biere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ebullition ADD CONSTRAINT FK_E57914BF7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE empatage ADD CONSTRAINT FK_214F1C4F7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('ALTER TABLE ingRecette ADD CONSTRAINT FK_EFE39C7BF7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ebullition');
        $this->addSql('DROP TABLE empatage');
        $this->addSql('DROP TABLE ingRecette');
    }
}

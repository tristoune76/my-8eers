<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210224180854 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere ADD date_brassage DATE DEFAULT NULL, ADD date_brassee DATE DEFAULT NULL, ADD vol_final_prevu DOUBLE PRECISION DEFAULT NULL, ADD tps_empatage DOUBLE PRECISION DEFAULT NULL, ADD vol_h2_oempatage DOUBLE PRECISION DEFAULT NULL, ADD vol_h2_orincage DOUBLE PRECISION DEFAULT NULL, ADD vol_houblonnage DOUBLE PRECISION DEFAULT NULL, ADD tps_houblonnage INT DEFAULT NULL, ADD d_initiale INT DEFAULT NULL, ADD vol_fermentation DOUBLE PRECISION DEFAULT NULL, ADD tps_fermentation INT DEFAULT NULL, ADD d_finale INT DEFAULT NULL, ADD sucre INT DEFAULT NULL, ADD vol_final_obtenu DOUBLE PRECISION DEFAULT NULL, ADD transition VARCHAR(255) NOT NULL, DROP ingredient_state');
        $this->addSql('ALTER TABLE palier DROP FOREIGN KEY FK_8EC5C57958866E23');
        $this->addSql('DROP INDEX IDX_8EC5C57958866E23 ON palier');
        $this->addSql('ALTER TABLE palier CHANGE brassage_id biere_id INT NOT NULL');
        $this->addSql('ALTER TABLE palier ADD CONSTRAINT FK_8EC5C579A71147CC FOREIGN KEY (biere_id) REFERENCES biere (id)');
        $this->addSql('CREATE INDEX IDX_8EC5C579A71147CC ON palier (biere_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere ADD ingredient_state TINYINT(1) NOT NULL, DROP date_brassage, DROP date_brassee, DROP vol_final_prevu, DROP tps_empatage, DROP vol_h2_oempatage, DROP vol_h2_orincage, DROP vol_houblonnage, DROP tps_houblonnage, DROP d_initiale, DROP vol_fermentation, DROP tps_fermentation, DROP d_finale, DROP sucre, DROP vol_final_obtenu, DROP transition');
        $this->addSql('ALTER TABLE palier DROP FOREIGN KEY FK_8EC5C579A71147CC');
        $this->addSql('DROP INDEX IDX_8EC5C579A71147CC ON palier');
        $this->addSql('ALTER TABLE palier CHANGE biere_id brassage_id INT NOT NULL');
        $this->addSql('ALTER TABLE palier ADD CONSTRAINT FK_8EC5C57958866E23 FOREIGN KEY (brassage_id) REFERENCES brassage (id)');
        $this->addSql('CREATE INDEX IDX_8EC5C57958866E23 ON palier (brassage_id)');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210312131505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient ADD type_ingredient_id INT NOT NULL');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870E3D257B9 FOREIGN KEY (type_ingredient_id) REFERENCES type_ingredient (id)');
        $this->addSql('CREATE INDEX IDX_6BAF7870E3D257B9 ON ingredient (type_ingredient_id)');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660E3D257B9 FOREIGN KEY (type_ingredient_id) REFERENCES type_ingredient (id)');
        $this->addSql('CREATE INDEX IDX_4B365660E3D257B9 ON stock (type_ingredient_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870E3D257B9');
        $this->addSql('DROP INDEX IDX_6BAF7870E3D257B9 ON ingredient');
        $this->addSql('ALTER TABLE ingredient DROP type_ingredient_id');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B365660E3D257B9');
        $this->addSql('DROP INDEX IDX_4B365660E3D257B9 ON stock');
    }
}

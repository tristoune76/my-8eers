<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210108122547 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient DROP FOREIGN KEY FK_6BAF7870F7CAAB59');
        $this->addSql('DROP INDEX IDX_6BAF7870F7CAAB59 ON ingredient');
        $this->addSql('ALTER TABLE ingredient DROP id_biere_id, DROP id_ingredient');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ingredient ADD id_biere_id INT NOT NULL, ADD id_ingredient INT NOT NULL');
        $this->addSql('ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870F7CAAB59 FOREIGN KEY (id_biere_id) REFERENCES biere (id)');
        $this->addSql('CREATE INDEX IDX_6BAF7870F7CAAB59 ON ingredient (id_biere_id)');
    }
}

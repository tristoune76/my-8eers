<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210304085311 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE etape_brassage (id INT AUTO_INCREMENT NOT NULL, etat INT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parametre_brassage (id INT AUTO_INCREMENT NOT NULL, etape_brassage_id INT NOT NULL, nom VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, label_link_etape VARCHAR(255) DEFAULT NULL, unite VARCHAR(255) DEFAULT NULL, helper_etape VARCHAR(255) DEFAULT NULL, commentaires LONGTEXT DEFAULT NULL, INDEX IDX_79F7AFDA3A6BEB10 (etape_brassage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parametre_brassage ADD CONSTRAINT FK_79F7AFDA3A6BEB10 FOREIGN KEY (etape_brassage_id) REFERENCES etape_brassage (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parametre_brassage DROP FOREIGN KEY FK_79F7AFDA3A6BEB10');
        $this->addSql('DROP TABLE etape_brassage');
        $this->addSql('DROP TABLE parametre_brassage');
    }
}

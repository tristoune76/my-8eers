<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201217184715 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere DROP FOREIGN KEY FK_D33ECD16AEE6030');
        $this->addSql('DROP INDEX IDX_D33ECD16AEE6030 ON biere');
        $this->addSql('ALTER TABLE biere ADD type_biere VARCHAR(255) NOT NULL, DROP type_biere_id');
        $this->addSql('ALTER TABLE type_biere DROP type_biere');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere ADD type_biere_id INT NOT NULL, DROP type_biere');
        $this->addSql('ALTER TABLE biere ADD CONSTRAINT FK_D33ECD16AEE6030 FOREIGN KEY (type_biere_id) REFERENCES type_biere (id)');
        $this->addSql('CREATE INDEX IDX_D33ECD16AEE6030 ON biere (type_biere_id)');
        $this->addSql('ALTER TABLE type_biere ADD type_biere VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}

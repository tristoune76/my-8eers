<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210518042807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE biere_register (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, date_ajout DATE NOT NULL, date_brassage DATE DEFAULT NULL, date_brassee DATE DEFAULT NULL, description LONGTEXT NOT NULL, state VARCHAR(255) NOT NULL, current_place VARCHAR(255) DEFAULT NULL, vol_final_prevu DOUBLE PRECISION DEFAULT NULL, tps_empatage DOUBLE PRECISION DEFAULT NULL, vol_h2_oempatage DOUBLE PRECISION DEFAULT NULL, vol_h2_orincage DOUBLE PRECISION DEFAULT NULL, vol_houblonnage DOUBLE PRECISION DEFAULT NULL, tps_houblonnage INT DEFAULT NULL, d_initiale INT DEFAULT NULL, vol_fermentation DOUBLE PRECISION DEFAULT NULL, tps_fermentation INT DEFAULT NULL, d_finale INT DEFAULT NULL, sucre INT DEFAULT NULL, vol_final_obtenu DOUBLE PRECISION DEFAULT NULL, transition VARCHAR(255) DEFAULT NULL, poids_grain DOUBLE PRECISION DEFAULT NULL, vol_empatage DOUBLE PRECISION DEFAULT NULL, d_rincage INT DEFAULT NULL, biere_id INT NOT NULL, date_log DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        // $this->addSql('DROP TABLE parametre_brassage');
        // $this->addSql('DROP TABLE type_ingredient');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->addSql('CREATE TABLE parametre_brassage (id INT AUTO_INCREMENT NOT NULL, etape_brassage_id INT NOT NULL, nom VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, label VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, label_link_etape VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, unite VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, helper_etape VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, commentaires LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_79F7AFDA3A6BEB10 (etape_brassage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        // $this->addSql('CREATE TABLE type_ingredient (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, unite VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, header VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ingredient_view_field LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', ingredient_field JSON DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        // $this->addSql('ALTER TABLE parametre_brassage ADD CONSTRAINT FK_79F7AFDA3A6BEB10 FOREIGN KEY (etape_brassage_id) REFERENCES etape_brassage (id)');
        $this->addSql('DROP TABLE biere_register');
    }
}

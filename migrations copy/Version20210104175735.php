<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210104175735 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere ADD volume_final DOUBLE PRECISION DEFAULT NULL, ADD volume_prevu DOUBLE PRECISION DEFAULT NULL, DROP vol_obtenu, DROP vol_voulu');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE biere ADD vol_obtenu DOUBLE PRECISION DEFAULT NULL, ADD vol_voulu DOUBLE PRECISION DEFAULT NULL, DROP volume_final, DROP volume_prevu');
    }
}

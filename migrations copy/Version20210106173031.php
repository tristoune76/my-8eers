<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210106173031 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock ADD ebcmin DOUBLE PRECISION DEFAULT NULL, ADD ebcmax DOUBLE PRECISION DEFAULT NULL, ADD acides_alpha_min DOUBLE PRECISION DEFAULT NULL, ADD acides_alpha_max DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock DROP ebcmin, DROP ebcmax, DROP acides_alpha_min, DROP acides_alpha_max');
    }
}

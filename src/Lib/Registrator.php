<?php

namespace App\Lib;

use App\Entity\StockRegister;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;

Class Registrator
{
    // private $entityManager;

    // public function __construct(EntityManagerInterface $entityManager)
    // {
    //     $this->entityManager = $entityManager;
    // }
    
    /**
     * Add item in the register
     */
    public function addInRegister($item)
    {
        // dd($this->entityManager);
        try 
        {
            $this->entityManager->persist($item);
            $this->entityManager->flush();
        }
        catch (ORMException $e)
        {
            dd($e);
        }
    }

    
}
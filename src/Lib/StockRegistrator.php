<?php


namespace App\Lib;

use App\Entity\Stock;
use App\Lib\Utilitaire;
use App\Lib\Registrator;
use App\Entity\StockRegister;
use DateTime;
use Symfony\Component\Yaml\Yaml;
use Doctrine\ORM\EntityManagerInterface;

class StockRegistrator extends Registrator
{
    protected $entityManager;
    private $utilitaire;
    private $parameters;    
    
    public function __construct(EntityManagerInterface $entityManager, Utilitaire $utilitaire)
    {
        $this->entityManager = $entityManager;
        $this->utilitaire = $utilitaire;
        $this->parameters = Yaml::parseFile('../config/parameters/stockRegistrator.yaml');
    }

    /**
     * Add item in register
     */
    public function addInStockRegister($stock, $reason)
    {
        
        $list = $this->utilitaire->parseAllPropertiesInObject($stock);
        $list['stockId'] = $list['id'];
        $list['description'] = $stock;
        $list['reason'] = $reason;
        $list['quantite'] = $stock->getQuantite();
        $list['userId'] =$stock->getUser()->getId();
        // dd($list);     
        $item = new StockRegister($list);
        // dd($item);



        $this->addInRegister($item);
        // $value = Yaml::parseFile('../config/parameters/brassor.yaml');
        // dd($value['brassageParam']);
    }

    /**
     * Faiseur de log
     */

     public function logMaker ($stock, $oldStock)
     {
        /**
         * Création d'un stock
         */ 
        if(!$stock->getId())
        {
            $log = "Creation du stock de ". $stock->getNom()." de type ".$stock->getTypeIngredient();
        }
        /**
         * Modification d'un stock
         */
        else
        {            
            if ($stock->getQuantite() != $oldStock->getQuantite())
            {
                $diff = $stock->getQuantite() - $oldStock->getQuantite();
                if ($diff > 0)
                {
                    $log = "Quantite +".$diff;
                }
                else
                {
                    $log = "Quantite -".-$diff;
                }
            }
            else
            {
                $log = "changement des caractéristiques du/de la ".$stock->getTypeIngredient();
            }
        }
        return $log;
     }

    /**
     * Read an item from the register
    */


    /**
     * Read all items for a stock from the stock register
     */
     public function readItemsInStockRegister($stock)
     {
        $items = $this->entityManager->getRepository(StockRegister::class)->findBy([
            "stockId"  =>  $stock->getId()
        ]); 
        $list = $this->utilitaire->parseListOfObject($items);
        return $list;
     }

    /**
     * Read all items from the stock register
     */
     public function readAllItemsInStockRegister()
     {
        $items = $this->entityManager->getRepository(StockRegister::class)->findAll();
        $list = $this->utilitaire->parseListOfObject($items);
        return $list;
     }

     /**
      * Delete item in register
      */

    
}
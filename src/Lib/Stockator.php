<?php

namespace App\Lib;

use App\Lib\Brassor;
use App\Entity\Biere;
use App\Entity\Stock;
use App\Lib\Validator;
use App\Lib\Utilitaire;
use App\Entity\Ingredient;

use function PHPSTORM_META\elementType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Stockator
{

    // private $biere;
    private $entityManager;
    private $session;
    private $brassor;
    private $validator;
    private $utilitaire;


    private $ingredientType = [
        'grain'  =>  [
            'nom' => 'grain',
            'description' =>  '',
            'larg' => 4,    //Nombre de largeur de colonne nécessaires pour les variables
            'variables'    => [
                [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'EBCMin',
                    'titre' =>  "EBC Min",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'EBCMax',
                    'titre' =>  "EBC Max",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en kg",
                    'helper'    =>  "",
                    'unite'  => 'Kg',
                    'larg'  =>  1
                ],
            ]
        ],
        'houblon'  =>  [
            'nom' => 'houblon',
            'description' =>  '',
            'larg'  =>  6,  //Nombre de largeur de colonne nécessaires pour les variables
            'variables'    => [
                [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1   //unité de largeur
                ],
                [
                    'nom'  => 'type',
                    'titre' =>  "Type de houblon",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'aromes',
                    'titre' =>  "Arômes",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'acidesAlphaMin',
                    'titre' =>  "Taux d'acides alpha minimum",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'acidesAlphaMax',
                    'titre' =>  "Taux d'acides alpha maximum",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en g",
                    'helper'    =>  "",
                    'unite'  => 'g',
                    'larg'  =>  1
                ],
            ]
        ],
        'levure'  =>  [
            'nom' => 'levure',
            'description' =>  '',
            'larg' =>   3, //Nombre de largeur de colonne nécessaires pour les variables
            'variables'    => [
                [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'type',
                    'titre' =>  "Type de levure",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en g",
                    'helper'    =>  "",
                    'unite'  => 'g',
                    'larg'  =>  1
                ],
            ]
        ],
        'additif'  =>  [
            'nom' => 'additif',
            'description' =>  '',
            'larg'  =>  4, //Nombre de largeur de colonne nécessaires pour les variables
            'variables'    => [
                [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en g",
                    'helper'    =>  "",
                    'unite'  => 'g',
                    'larg'  =>  1
                ],
                [
                    'nom'  => 'description',
                    'titre' =>  "Description de l'additif",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  =>  2
                ],
            ]
        ],
    ];


    
    /**
     * __construct
     *
     * @param  mixed $entityManager
     * @param  mixed $session
     * @param  mixed $brassor
     * @param  mixed $validator
     * @return void
     */
    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Brassor $brassor, Validator $validator, Utilitaire $utilitaire)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->brassor = $brassor;
        $this->validator = $validator;
        $this->utilitaire = $utilitaire;
    }

    public function retrieveStock ($typeIngredient)
    {
        $stock = $this->entityManager->getRepository(Stock::class)->findBy([
            'typeIngredient'  =>  $typeIngredient
        ]);
        return $stock;
    }

    public function isStockForIngredient (Ingredient $ingredient)
    {
        $stocks = $this->retrieveStock($ingredient->getTypeIngredient());
        if (!$stocks)
        {
            $notification = "Il n'y a pas de " . strtolower($ingredient->getTypeIngredient()) . " en stock\n";
            $status = 'danger';
            $this->utilitaire->notify($notification, $status);
        }
        return $stocks;
        
    }

    public function is_Enough($stock, $retraitSTock)
    {
        $diff = $stock->getQuantite() - $retraitSTock;
        $unite = $this->brassor->getIngredientType()[$stock->getTypeIngredient()]['variables']['quantite']['unite'];
        if ($diff < 0) // il n'y a pas assez de stock
        {
            $notification = "Il n'y a pas assez de stock pour du " . \strtolower($stock->getNom())  . ". il vous manque " . -$diff . " " . $unite . "\n";
            $status = 'danger';
        } else    // Il y assez de stock
        {
            if ($retraitSTock > 0) {
                $notification = $retraitSTock . $unite . " de " . \strtolower($stock->getNom()) . " ont été retirés du stock.";
            } else {
                $notification = -$retraitSTock . $unite . " de " . \strtolower($stock->getNom())  . " ont été remis en stock.";
            }
            $notification .= " Il reste " . $diff . $unite . ".\n";
            $status = 'success';

            $stock->setQuantite($diff);
        }
        $retour = [
            'notification'  => $notification,
            'status'        => $status
        ];
        return $retour;
    }

     


    /**
     * retrieveOneIngredientFromStock
     *
     * @param  mixed $ingredient
     * @return void
     */
    public function retrieveOneIngredientFromStock(Ingredient $ingredient, $quantiteInitiale = 0)
    {
        /**
         * Fonction de retrait du stock d'un ingrédient
         * - On calcule ce qu'il faut retirer du stock
         * - On regarde s'il y a assez de quantité en stock
         *      - Si oui : retrait du stock + message de réussite
         *      - Si non : pas de retrait du stock + message d'échec
         *
         */

        $stock = $ingredient->getStock();
        //Vérification qu'il y a assez de stock
        // $diff = $stock->getQuantite() - $ingredient->getQuantite();
        $unite = $this->brassor->getIngredientType()[$stock->getTypeIngredient()]['variables']['quantite']['unite'];
        
        if ($ingredient->getIsBrewed() == Brassor::INGREDIENTS_PREVUS)  // si on retire du stock à la suite de la création de la recette, avant le brassage
        {
            $retraitSTock = $ingredient->getQuantite();
            return $this->is_Enough($stock, $retraitSTock);
        }
        else    // si on retire du stock pendant le brassage (modification d'un ingrédient déjà dans la recette ou ajout d'un ingrédient)
        {
            if ($ingredient->getId())   //Modification d'un ingrédient déjà prévu
            {
                $retraitSTock = $ingredient->getQuantite() - $quantiteInitiale;
                $retour = $this->is_Enough($stock, $retraitSTock);                             
            }
            else    //Ajout d'un ingrédient
            {
                $retraitSTock = $ingredient->getQuantite();
                $retour = $this->is_Enough($stock, $retraitSTock);
                if ($retour['status'] != 'danger') {
                    $this->entityManager->persist($ingredient);
                } 
            }
            if ($retour['status'] == 'success' && $ingredient->getStock()->getTypeIngredient() == 'grain') {
                $biere = $this->entityManager->getRepository(Biere::class)->findOneBy(['id'  =>  $ingredient->getBiereId()]);
                $biere->setPoidsGrain($biere->getPoidsGrain() + $retraitSTock);
            }   
            
            return $retour;                               
        }
    }

    public function retrieveAllIngredientsFromStock($ingredients)
    {
        $messageSuccess = '';
        $messageDanger = '';
        $status = "success";
        for ($i = 0; $i < count($ingredients); $i++) 
        {
            $retour = $this->retrieveOneIngredientFromStock($ingredients[$i]);
            if ($retour['status'] == 'danger') 
            {
                $messageDanger .= $retour['notification'];
                $status = "danger";
            }
            else
            {
                $messageSuccess .= $retour['notification'];
                $messageDanger .= "Il y a assez de " . $ingredients[$i]->getStock()->getNom() . "\n";
            }            
        }
        if ($status == "danger")
        {
            return [
                'notification'  =>  $messageDanger,
                'status'    =>  $status
            ];
        }
        else
        {
            return [
                'notification'  =>  $messageSuccess,
                'status'    =>  $status
            ];
        }
    }

    
    
    /**
     * retrieveFromStock
     *
     * @param  mixed $biereId
     * @return void
     */
    public function retrieveFromStock($biereId)
    {
        $biere = $this->brassor->getBiereById($biereId);
        $biereNom = $biere->getNom();
        
        // Si la recette est complète
        if ($this->validator->testCompletudeIngredientForBeer($biereId))
        {
            $ingredients = $this->entityManager->getRepository(Ingredient::class)->findBy([
                'biereId'   =>  $biereId,
                'is_brewed'  =>  Brassor::INGREDIENTS_PREVUS
            ]);
            
            /**Retrait des ingrédients dans le stock
             * Renvoie de la notification a affiché et du statut de réussité. 
             * Si danger alos au moins un des ingrédients n'est pas présent en stock suffisamment
             * Si success alors tous les ingrédients sont présens en quantité suffisante
             */
            $retour = $this->retrieveAllIngredientsFromStock($ingredients);
            $status = $retour['status'];          

            // S'il n'y a pas assez de stock pour au moins un des ingrédient
            if ($status == "danger")
            {
                $notification = "Certains stocks sont insuffisants.\n" . "Les ingrédients n'ont pas été retirés du stock.\n";
                $notification .= $retour['notification'];

                //Envoi du message de notification en variable de session
                $this->utilitaire->notify($notification, $status);
                return false;
            } 
            else        //S'il y a assez de stock pour tous les ingrédients => mise à jour des quantités en stock
            {
                $notification = "Les ingrédients de la bière " . $biereNom . " ont été retirés du stock.\n" . "Les ingrédients sont validés. Vous pouvez brasser!!\n";
               
                /**
                 * Copie des ingrédients et indication qu'ils sont en cours de brassage
                 */
                foreach ($ingredients as $ingredient)
                {
                    $newIngredient = $ingredient->copyObject();
                    $newIngredient->setIsBrewed(true);
                    $this->entityManager->persist($newIngredient);

                }

                /**
                 * calcul du poids du grain dans la recette
                 */
                $this->brassor->setPoidsGrain($biereId);

                /*
                * inscription en DB
                */
                $this->entityManager->flush();
                $notification .= $retour['notification'];
                // dd($newIngredient, $ingredient);

                //Envoi du message de notification en variable de session
                $this->utilitaire->notify($notification, $status);  
                return true;
            }
            
        }        
    }

    
    /**
     * replaceInStock
     *
     * @param  mixed $biereId
     * @return void
     */
    public function replaceInStock($biereId)
    {
        $biere = $this->brassor->getBiereById($biereId);

        // $ingredients = $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS);
        $ingredients = $this->entityManager->getRepository(Ingredient::class)->findBy([
            'biereId' =>  $biereId
        ]);
        $notification = "";      //création du message à afficher pour indique ce qui a été fait
        foreach($ingredients as $ingredient)
        {
            if ($ingredient->getIsBrewed() == brassor::INGREDIENTS_REELS)
            {
                $this->entityManager->remove($ingredient);
                $this->entityManager->flush();
            }
            else
            {
                $quantiteNecessaire = $ingredient->getQuantite();
                $stockIngredient = $ingredient->getStock();
                $quantiteStockInitiale = $stockIngredient->getQuantite();
                $nomStock = $stockIngredient->getNom();
                $unite = $this->brassor->getIngredientType()[$stockIngredient->getTypeIngredient()]['variables']['quantite']['unite'];

                $quantiteStockFinale = $quantiteStockInitiale + $quantiteNecessaire;

                $notification .= "Le stock de " . \strtolower($nomStock) . " a été reapprovisionné de " . $quantiteNecessaire . $unite . ". Il reste " . $quantiteStockFinale . $unite . ".\n";

                $stockIngredient->setQuantite($quantiteStockFinale);
                $this->entityManager->remove($ingredient);
                $this->entityManager->flush();
            }
        }

        $biere->setPoidsGrain(0);   // Remise à 0 du poids du grain
        $this->entityManager->flush();

        $status = 'success';
        $this->utilitaire->notify($notification, $status);        
    }

    /**
     * Get the value of ingredientType
     */ 
    public function getIngredientType()
    {
        return $this->ingredientType;
    }

    /**
     * Set the value of ingredientType
     *
     * @return  self
     */ 
    public function setIngredientType($ingredientType)
    {
        $this->ingredientType = $ingredientType;

        return $this;
    }
}


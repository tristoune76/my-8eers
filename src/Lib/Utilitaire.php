<?php

namespace App\Lib;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

Class Utilitaire
{
    private $session;
    
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    
    public function notify ($notification, $status = null, $buttons = null)
    {
        $message = [
            'notification'  =>  $notification,
            'status'        =>  $status,
            'buttons'        =>  $buttons
        ];
        $this->session->set('message', $message);
    }

    public function copyObject($original)
    {
        $class = get_class($original);

        $vars = get_class_vars($class);
        $newObject = new $class;
        foreach ($vars as $var => $data) {
            $setMethode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));
            $getMethode = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));

            if (is_callable(array($original, $getMethode)) && is_callable(array($original, $setMethode))) {
                $newObject->$setMethode($original->$getMethode());
            }
        }
        return $newObject;
    }

    public function parseAllPropertiesInObject($object)
    {
        $vars = $object->getVars();
        foreach ($vars as $var => $data) {
            $getMethode = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));
            $list[$var] = $object->$getMethode();
        }
        return $list;
    }

    public function parseSelectedPropertiesInObject($object, $properties)
    {
        // $vars = $object->getVars();
        foreach ($properties as $var => $data) {
            $getMethode = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $data)));
            $list[$data] = $object->$getMethode();
        }
        return $list;
    }

    public function parseListOfObject($objects)
    {
        $list = [];
        for ($i = 0; $i < count($objects); $i++) {
            $list[$i] = $this->parseAllPropertiesInObject($objects[$i]);
        }
        return $list;
    }
}
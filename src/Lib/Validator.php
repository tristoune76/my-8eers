<?php

namespace App\Lib;

use App\Entity\Biere;
use App\Lib\Brassor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class Validator
{
    private $entityManager;
    private $session;
    private $brassor;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Brassor $brassor)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->brassor = $brassor;
    }




    public function testCompletudeIngredientForBeer($biereId)
    {
        $ingredients = $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS);
        $variables = $this->brassor->getTypeIngredientsEssentiels();
        // dd($variables);
        $isComplete = true;
        $notification = "";
        foreach ($variables as $variable) 
        {
            if (count($ingredients[$variable]) == 0) 
            {
                $status = "danger";
                $isComplete = false;
                $notification .= "Choisissez un(e) " . $variable . " pour de terminer votre recette.\n";
            }
        }
        if (!$isComplete)
        {
            $this->utilitaire->notify($notification, $status);
        }
        return $isComplete;
    }

    public function volH2OEmpatageValidator(Biere $biere)
    {
        $message = '';
        $taux = $biere->getVolH2OEmpatage() / $biere->getPoidsGrain();

        if ($taux > $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['volH2OEmpatage']['valMax']) {
            $message = "Le volume d'eau d'empatage semble trop important. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['volH2OEmpatage']['valMin'] . " l/kg et " . $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['volH2OEmpatage']['valMax'] . " l/kg"."\n";

        } elseif ($taux < $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['volH2OEmpatage']['valMin']) {
            $message = "Le volume d'eau d'empatage semble trop faible. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['volH2OEmpatage']['valMin'] . " l/kg et " . $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['volH2OEmpatage']['valMax'] . " l/kg"."\n";

        }


        return $message;
    }

    public function volH2ORincageValidator(Biere $biere)
    {
        $message = '';
        $taux = $biere->getVolH2ORincage() / $biere->getPoidsGrain();

        if ($taux > $this->brassor->getBrassageParam()[Biere::EMPATEE]['variables']['volH2ORincage']['valMax']) {
            $message = "Le volume d'eau d'empatage semble trop important. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::EMPATEE]['variables']['volH2ORincage']['valMin'] . " l/kg et " . $this->brassor->getBrassageParam()[Biere::EMPATEE]['variables']['volH2ORincage']['valMax'] . " l/kg"."\n";

        } elseif ($taux < $this->brassor->getBrassageParam()[Biere::EMPATEE]['variables']['volH2ORincage']['valMin']) {
            $message = "Le volume d'eau d'empatage semble trop faible. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::EMPATEE]['variables']['volH2ORincage']['valMin'] . " l/kg et " . $this->brassor->getBrassageParam()[Biere::EMPATEE]['variables']['volH2ORincage']['valMax'] . " l/kg"."\n";

        } 

        return $message;
    }

    public function dInitialeValidator(Biere $biere)
    {
        $message = '';

        if ($biere->getDInitiale > $this->brassor->getBrassageParam()[Biere::HOUBLONNEE]['variables']['dInitiale']['valMax']) {
            $message = "La densité initiale semble trop importante. ";
            $message .= "Elle doit être comprise entre " . $this->brassor->getBrassageParam()[Biere::HOUBLONNEE]['variables']['dInitiale']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::HOUBLONNEE]['variables']['dInitiale']['valMax']."\n";

        } elseif ($biere->getDInitiale < $this->brassor->getBrassageParam()[Biere::HOUBLONNEE]['variables']['dInitiale']['valMin']) {
            $message = "La densité initiale semble trop faible. ";
            $message .= "Elle doit être comprise entre " . $this->brassor->getBrassageParam()[Biere::HOUBLONNEE]['variables']['dInitiale']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::HOUBLONNEE]['variables']['dInitiale']['valMax']."\n";

        }

        return $message;
    }

    public function dFinaleValidator(Biere $biere)
    {
        $message = '';

        if ($biere->getDFinale > $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['dFinale']['valMax']) {
            $message = "La densité finale semble trop importante. ";
            $message .= "&nbsp;Elle doit être comprise entre " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dFinale']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dFinale']['valMax'] . "\n";

        } elseif ($biere->getDFinale < $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['dFinale']['valMin']) {
            $message = "La densité finale semble trop faible. ";
            $message .= "&nbsp;Elle doit être comprise entre " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dFinale']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dFinale']['valMax'] . "\n";

        }

        return $message;
    }

    public function dRincageValidator(Biere $biere)
    {
        $message = '';

        if ($biere->getDRincage > $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['dRincage']['valMax']) {
            $message = "La densité après rinçage semble trop importante. ";
            $message .= "&nbsp;Elle doit être comprise entre " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dRincage']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dRincage']['valMax'] . "\n";
        } elseif ($biere->getDRincage < $this->brassor->getBrassageParam()[Biere::LANCEE]['variables']['dRincage']['valMin']) {
            $message = "La densité après rinçage semble trop faible. ";
            $message .= "&nbsp;Elle doit être comprise entre " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dRincage']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['dRincage']['valMax'] . "\n";
        }

        return $message;
    }

    public function tpsHoublonnageValidator(Biere $biere)
    {
        $message = '';

        if ($biere->getTpsHoublonnage() > $this->brassor->getBrassageParam()[Biere::RINCEE]['variables']['tpsHoublonnage']['valMax']) {
            $message = "Le temps d'ébullition semble faible. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::RINCEE]['variables']['tpsHoublonnage']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::RINCEE]['variables']['tpsHoublonnage']['valMax'] . " mn." . "\n";

        } elseif ($biere->getTpsHoublonnage() < $this->brassor->getBrassageParam()[Biere::RINCEE]['variables']['tpsHoublonnage']['valMin']) {
            $message = "Le temps d'ébullition semble important. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::RINCEE]['variables']['tpsHoublonnage']['valMin'] . " et " . $this->brassor->getBrassageParam()[Biere::RINCEE]['variables']['tpsHoublonnage']['valMax'] ." mn.". "\n";

        }
        return $message;
    }

    public function sucreValidator(Biere $biere)
    {
        $message = '';

        if ($biere->getSucre() > $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['sucre']['valMax']) {
            $message = "Le taux de sucre semble faible. ";
            $message .= "Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['sucre']['valMin'] . " g/l et " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['sucre']['valMax'] . "g/l" . "\n";

        } elseif ($biere->getSucre() < $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['sucre']['valMin']) {
            $message = "Le taux de sucre semble important. ";
            $message .= ";Il doit être compris entre " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['sucre']['valMin'] . " g/l et " . $this->brassor->getBrassageParam()[Biere::FERMENTEE]['variables']['sucre']['valMax'] . "g/l" . "\n";

        }
        return $message;
    }

    public function palierValidator($biere)
    {
        $message = '';
        $paliers = $this->brassor->getPaliersByIdBiere($biere->getId());
        if (count($paliers) == 0)
        {
            $message .= "Aucun palier de température n'est défini. ";
        }
        else
        {
            $tpsTotal = 0;
            for ($i=0; $i< count($paliers); $i++)
            {
                if ($paliers[$i]->getTemps() > 90)
                {
                    $message .= "Le palier n°".$i." semble être long (Tps > 90mn). ";
                }
                $tpsTotal += $paliers[$i]->getTemps();
            }
            if ($tpsTotal > 90)
            {
                $message .= "Le temps total d'empatage semble être long (Tps > 90mn). ";
            }
        }
        return $message;
    }

    public function stateValidator($stateView, Biere $biere)
    {
        $message = '';
        
        foreach($this->brassor->getBrassageParam()[$stateView]['variables'] as $variable) {
            $methode = 'get'.ucfirst($variable['nom']);
            if(!$biere->$methode())
            {
                $message .= "Vous n'avez pas saisi ".lcfirst($variable['titre'])."\n";
            }
            else
            {
                $methode = lcfirst($variable['nom']).'Validator';
                if (is_callable(array($this, $methode))) {
                    $message .= $this->$methode($biere);
                }
            }            
        }
        // dd($message);
        return $message;
    }
    
}
<?php

namespace App\Lib;

use App\Lib\Stockator;
use App\Lib\Utilitaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Notificator extends AbstractController
{
    
    
    private $entityManager;
    private $session;
    private $stockator;
    private $utilitaire;


    private $message = [
        "0001"    =>  [
            "notification"  =>  "stockController->forms, ingredientType à null pour stock à null.",
            "status"        =>  "danger"
        ]
    ];

    // private $notification;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Stockator $stockator, Utilitaire $utilitaire)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->stockator = $stockator;
        $this->utilitaire = $utilitaire;
    }

    public function homeReturn($id)
    {
        $this->messageNotifiy($id);
        return $this->redirectToRoute('home');
    }

    public function messageNotifiy($id)
    {
        if (isset($this->message[$id])) {
            $notification = "Message n°" . $id . ".";

            isset($this->message[$id]['notification']) ? $notification .= $this->message[$id]['notification'] : null;
            isset($this->message[$id]['status']) ? $status = $this->message[$id]['status'] : null;
            isset($this->message[$id]['buttons']) ? $buttons = $this->message[$id]['buttons'] : null;
        } else {
            $notification = "pas de message n°" . $id . ".";
            $status = null;
            $buttons = null;
        }
        $this->utilitaire->notify($notification, $status, $buttons);
    }

    /**
     * Get the value of message
     */ 
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set the value of message
     *
     * @return  self
     */ 
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
}
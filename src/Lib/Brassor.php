<?php

namespace App\Lib;

use App\Lib\Utility;
use App\Entity\Biere;
use App\Entity\Palier;

use App\Entity\Ingredient;
use App\Lib\TypeOfIngredient;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Brassor
{

    // private $biere;
    private $entityManager;
    private $session;


    // private $etapes = [
    //     Biere::PRETE     =>  'Prète',
    //     Biere::LANCEE    =>  'Brassage lancé',
    //     Biere::EMPATEE   =>  'Empatée',
    //     Biere::RINCEE    =>  'Rinçée',
    //     Biere::HOUBLONNEE    =>  'Houblonnée',
    //     Biere::FERMENTEE     =>  'Fermentée',
    //     // Biere::EMBOUTEILLEE  =>  'embouteillée',
    //     Biere::BRASSEE       =>  'Brassée',
    //     Biere::STOP => 'Arrêtée'
    // ];

    const INGREDIENTS_REELS = true;
    const INGREDIENTS_PREVUS = false;

    // private $transitions = [
    //     Biere::PRETE    => 'Prète',
    //     Biere::LANCEE   =>  'Empatage',
    //     Biere::EMPATEE   =>  'Rinçage',
    //     Biere::RINCEE    =>  'Houblonnage',
    //     Biere::HOUBLONNEE    =>  'Fermentation',
    //     Biere::FERMENTEE     =>  'Embouteillage',
    //     // Biere::EMBOUTEILLEE  => 'embouteillage',
    //     Biere::BRASSEE     => 'Brassée'
    // ];

    private $brassageParam = [
        Biere::PROJET   =>  [
            'nom' =>  'Préparation',
            'etape'      =>  'En projet',
            'description' => "Description de l'étape",
            'variables' => [
                'volFinalPrevu' =>  [
                    'nom'  => 'volFinalPrevu',
                    'titre' =>  "Le volume prévu de ma bière",
                    'helper'    =>  "",
                    'unite'  => 'litres',
                    'action' => 'Définir'                    
                ],
                'Ingredients'   =>  [
                    'nom'  => 'Ingredients',
                    'titre' =>  "Les ingrédients",
                    'helper'    =>  "",
                    'unite' => '',
                    'action' => 'Ajouter'
                ]
            ]
        ],
        Biere::PRETE   =>  [
            'nom' =>  'Description',
            'etape'      =>  'Prète',
            'description' => "Description de l'étape",
            'variables' => [
                'volFinalPrevu' =>  [
                    'nom'  => 'volFinalPrevu',
                    'titre' =>  "Le volume prévu de ma bière",
                    'helper'    =>  "",
                    'unite'  => 'litres',
                    'action' => false
                ],
                'Ingredients'   =>  [
                    'nom'  => 'Ingredients',
                    'titre' =>  "Les ingrédients",
                    'helper'    =>  "",
                    'unite' => '',
                    'action' => false
                ]
            ]
        ],
        Biere::LANCEE  =>  [
            'nom' => 'Empatage',
            'etape'      =>  'Lancée',
            'description' => "Description de l'étape",
            'variables' => [
                'poidsGrain'    =>  [
                    'nom'  => 'poidsGrain',
                    'titre' =>  "Le poids total de tous les grains",
                    'helper'    =>  "",
                    'unite'  => 'kg',
                    'action' => false   // pas de formulaire -> juste une information
                ],
                'volH2OEmpatage'    =>  [
                    'nom'  => 'volH2OEmpatage',
                    'titre' =>  "Le volume d'eau pour l'empatage",
                    'helper'    =>  "Entre 2 et 3 litres d'eau par kg de grain",
                    'unite'  => 'litres',
                    'action' => 'Définir',
                    'valMin'    =>  2,    // taux volume d'eau par rapport au poids de grain
                    'valMax'    =>  3     // taux volume d'eau par rapport au poids de grain
                ],
                'Paliers'   =>  [
                    'nom'  => 'Paliers',
                    'titre' =>  "Les paliers",
                    'helper'    =>  "", 
                    'unite' => '',
                    'action' => 'Ajouter'
                ]
            ]
        ],
        Biere::EMPATEE   =>  [
            'nom' => 'Rinçage',
            'etape'      =>  'Empatée',
            'description' => "Description de l'étape",
            'variables' => [
                // 'volEmpatage' =>  [
                //     'nom'  => 'volEmpatage',
                //     'titre' =>  "Le volume de mout à la fin de l'empatage",
                //     'helper'    =>  "",
                //     'unite'  => 'litres',
                //     'action' => 'Définir'

                // ],
                'poidsGrain'    =>  [
                    'nom'  => 'poidsGrain',
                    'titre' =>  "Le poids total de tous les grains",
                    'helper'    =>  "",
                    'unite'  => 'kg',
                    'action' => false   // pas de formulaire -> juste une information
                ],
                'volH2ORincage' => [
                    'nom'  => 'volH2ORincage',
                    'titre' =>  "Le volume d'eau pour le rinçage",
                    'helper'    =>  "Entre 2 et 3 litres d'eau par kg de grain", 
                    'unite'  => 'litres',
                    'action' => 'Définir',
                    'valMin'    =>  2,    // taux volume d'eau par rapport au poids de grain
                    'valMax'    =>  3     // taux volume d'eau par rapport au poids de grain

                ],
                'dRincage'  =>  [
                    'nom'  => 'dRincage',
                    'titre' =>  "La densité après le rinçage",
                    'helper'    =>  "A mesurer à la suite du rinçage juste avant l'ébullition",
                    'unite'  => '',
                    'action' => 'Mesurer',
                    'valMin'    =>  1000,
                    'valMax'    =>  1100
                ],                
            ]
        ],
        Biere::RINCEE   =>  [
            'nom' => 'Houblonnage',
            'etape'      =>  'Rinçée',
            'description' => "Description de l'étape",
            'variables' => [
                    'volHoublonnage'  =>  [
                        'nom'  => 'volHoublonnage',
                        'titre' =>  "Le volume de moût avant houblonnage",
                        'helper'    =>  "",
                        'unite'  => 'litres',
                        'action' => 'Mesurer'
                    ],                    
                    'tpsHoublonnage'    =>  [
                        'nom'  => 'tpsHoublonnage',
                        'titre' =>  "Le temps de houblonnage",
                        'helper'    =>  "", 
                        'unite'  => 'minutes',
                        'action' => 'Définir',
                        'valMin'    =>  30,
                        'valMax'    =>  90
                    ]
                ]
        ],
        Biere::HOUBLONNEE   =>  [
            'nom' => 'Fermentation',
            'etape'      =>  'Houblonnée',
            'description' => "Description de l'étape",
            'variables' =>   [
                    'dInitiale'   =>  [
                        'nom'  => 'dInitiale',
                        'titre' =>  "La densité initiale",
                        'helper'    =>  "A mesurer à la suite de l'ébulition. Valeur à 20°C sinon ajuster.", 
                        'unite'  => '',
                        'action' => 'Mesurer',
                        'valMin'    =>  1000,
                        'valMax'    =>  1100

                    ],
                    'volFermentation' =>  [
                        'nom'  => 'volFermentation',
                        'titre' =>  "Le volume de moût avant la fermentation",
                        'helper'    =>  "", 
                        'unite'  => 'litres',
                        'action' => 'Mesurer'

                    ]
                ]
        ],
        Biere::FERMENTEE   =>  [
            'nom' => 'Embouteillage',
            'etape'      =>  'Fermentée',
            'description' => "Description de l'étape",
            'variables' =>   [
                    'dFinale'    =>  [
                        'nom'  => 'dFinale',
                        'titre' =>  "La densité finale",
                        'helper'    =>  "A mesurer à la suite de la fermentation. Valeur à 20°C sinon ajuster", 
                        'unite'  => '',
                        'action' => 'Mesurer',
                        'valMin'    =>  1000,
                        'valMax'    =>  1100

                    ],
                    'tpsFermentation' =>  [
                        'nom'  => 'tpsFermentation',
                        'titre' =>  "Le temps de fermentation",
                        'helper'    =>  "Fermentation primaire jusqu'à ce que le barboteur ne bulle plus. Fermentation secondaire environ 10 jours (Quand la densité est au minimum).",
                        'unite'  => 'jours',
                        'action' => 'Mesurer'

                    ],
                    'sucre'   =>  [
                        'nom'  => 'sucre',
                        'titre' =>  "La quantité de sucre à rajouter",
                        'helper'    =>  "Environ 7g/litre de moût. Un calcul de carbonation peut permettre d'affiner",
                        'unite'  => 'g/litre',
                        'action' => 'Définir',
                        'valMin'    =>  5,
                        'valMax'    =>  8
                    ],
                    'volFinalObtenu'  =>  [
                        'nom'  => 'volFinalObtenu',
                        'titre' =>  "Le volume de bière finalement obtenu",
                        'helper'    =>  "",
                        'unite'  => 'litres',
                        'action' => 'Mesurer'
                    ]
                ] 
        ],
        // Biere::EMBOUTEILLEE   =>  [
        //     'nom' => '',
        //     'etape'      =>  'Embouteillée',
        //     'description' => "Description de l'étape",
        //     'variables' => []
        // ],
        Biere::BRASSEE   =>  [
            'nom' => 'Fiche Bière',
            'etape'      =>  'Brassée',
            'description' => "Description de l'étape",
            'variables' => []
        ],
        Biere::STOP     =>  [
            'nom' => 'Arrêt du brassage',
            'etape'      =>  'Arrêtée',
            'description' => "Description de l'étape",
            'variables' => []
        ]
    ];

    // private $typeIngredientsEssentiels = ['grain', 'houblon', 'levure'];
    private $typeIngredientsEssentiels = [];


    private $ingredientType = [
        'grain'  =>  [
            'nom' => 'grain',
            'larg' => 2,    //Nombre de largeur de colonne nécessaires pour les variables
            'essentiel'  => true,
            'variables'    => [
                'nom' => [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  => 1
                ],
                'quantite' => [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en kg",
                    'helper'    =>  "",
                    'unite'  => 'kg',
                    'larg'  => 1
                ],
            ]
        ],
        'houblon'  =>  [
            'nom' => 'houblon',
            'larg' => 3,    //Nombre de largeur de colonne nécessaires pour les variables
            'essentiel'  => true,
            'variables'    => [
                'nom' => [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  => 1
                ],
                'quantite' => [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en g",
                    'helper'    =>  "",
                    'unite'  => 'g',
                    'larg'  => 1
                ],
                'tempsEbulltion' => [
                    'nom'  => 'tempsEbullition',
                    'titre' =>  "Ebullition en mn",
                    'helper'    =>  "",
                    'unite'  => 'mn',
                    'larg'  => 1
                ],
            ]
        ],        
        'levure'  =>  [
            'nom' => 'levure',
            'larg' => 2,    //Nombre de largeur de colonne nécessaires pour les variables
            'essentiel'  => true,
            'variables'    => [
                'nom' => [
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  => 1
                ],
                'quantite' => [
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en g",
                    'helper'    =>  "",
                    'unite'  => 'g',
                    'larg'  => 1
                ],
            ]
        ],
        'additif'  =>  [
            'nom' => 'additif',
            'larg' => 3,    //Nombre de largeur de colonne nécessaires pour les variables
            'essentiel'  => false,
            'variables'    => [
                'nom'=>[
                    'nom'  => 'nom',
                    'titre' =>  "Nom",
                    'helper'    =>  "",
                    'unite'  => '',
                    'larg'  => 1
                ],
                'quantite'=>[
                    'nom'  => 'quantite',
                    'titre' =>  "Quantité en g",
                    'helper'    =>  "",
                    'unite'  => 'g',
                    'larg'  => 1
                ],
                'tempsEbulltion'=>[
                    'nom'  => 'tempsEbullition',
                    'titre' =>  "Ebullition en mn",
                    'helper'    =>  "",
                    'unite'  => 'mn',
                    'larg'  => 1
                ]
            ]
        ]
    ];

    private $messageButtons = [
        "button1" => [
            "path" => [
                'path'      => 'brassageContinue',
                'variables' => [
                    'biereId',
                    'stateView' 
                ]
            ],        
            'titre' =>  'Annuler'
        ],
        "button2" => [
            "path" => [
                'path'      => 'brassageFwd',
                'variables' =>  [
                    'biereId',
                    'stateView' 
                ]
            ],        
            'titre' =>  'Valider'
        ]
    ];
                    

    // private $ingredientType = ['houblon', 'grain', 'levure', 'additifs'];
    

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->typeIngredientsEssentiels = $this->setTypeIngredientsEssentiels();
    }

    
    // /**
    //  * @param Request $request 
    //  * @return mixed 
    //  */
    // public function volFinalPrevu (Request $request)
    // {
    //     $biere = $this->getBiereById($id);
    //     $biere = $this->getBiereById($id);

    //     if (!$biere) {
    //         $biere = new Brassage($biere);
    //     }

    //     $volFinalPrevu = $biere->getVolPrevu();
    //     if ($volFinalPrevu) {
    //         $placeholder = $volFinalPrevu . ' Litres';
    //     } else {
    //         $placeholder = 'En litre';
    //     }

    //     $form = $this->createForm(VolPrevuType::class, $biere, [
    //         'placeholder'   =>  $placeholder
    //     ]);
    //     $form->handleRequest($request);
    //     $notification = "Quel volume final du brassin voulez vous?";

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->entityManager->flush();
    //         $notification = 'Le volume de ma bière sera de ' . $biere->getVolPrevu() . ' litres';
    //         return $this->render('biere/volFinalPrevu.html.twig', [
    //             'biere'  =>  $biere,
    //             //                    'TypesOfIngredient' =>  $this->typesOfIngredient,
    //             'notification'   =>  $notification
    //         ]);
    //     }

    //     return $this->render('biere/volFinalPrevu.html.twig', [
    //         'form'  =>  $form->createView(),
    //         'biere'  =>  $biere,
    //         //            'TypesOfIngredient' =>  $this->typesOfIngredient,
    //         'notification'   =>  $notification
    //     ]); 
    // }

    public function removePalier($palier, $biere)
    {
        try {
            $this->entityManager->remove($palier);
            $this->entityManager->flush();

            $biere->setTpsEmpatage($biere->getTpsEmpatage() - $palier->getTemps());
            $this->entityManager->flush();
        } catch (ORMException $e) {
            // $this->session->add('errors', $e);
        }        
    }
    

    /**
     * getBiereById
     *
     * @param  mixed $id
     * @return Biere
     */
    public function getBiereById($idBiere)
    {
        return $this->entityManager->getRepository(Biere::class)->findOneBy([
            'id'   =>  $idBiere
        ]);
    }

    /**
     * getPalierByIdBiere
     *
     * @param  mixed $idBiere
     * @return Array
     */
    public function getPaliersByIdBiere($idBiere)
    {
        // $typesOfIngredient = $this->typeOfIngredient->getTypeOfIngredient();
        return $this->entityManager->getRepository(Palier::class)->findBy([
            'biere'   =>  $this->getBiereById($idBiere)
        ]);
    }

    /**
     * getIngredientsByIdBiere
     *
     * @param  mixed $idBiere
     * @return Array
     */
    public function getIngredientsByIdBiere($idBiere, $reel)
    {
        $ingredientList = $this->entityManager->getRepository(Ingredient::class)->findBy([
            'biereId'   =>  $idBiere,
            'is_brewed'  =>  $reel
        ]);
        $ingredients = [];
        foreach ($this->ingredientType as $type) 
        {
            $ingredients[$type['nom']] = [];
            foreach($ingredientList as $ingredient)
            {
                // if ($ingredient->getIsBrewed() == $reel)
                // {
                    
                    if ($ingredient->getTypeIngredient() == $type['nom']) 
                    {
                        $item['id'] = $ingredient->getId();
                        $item['nom'] = $ingredient->getStock()->getNom();
                        $item['quantite'] = $ingredient->getQuantite();
                        $item['tempsEbullition'] = $ingredient->getTempsEbullition();
                        $ingredients[$type['nom']][] = $item;
                    }
                // }                
            }            
        }
        return $ingredients;
    }

    public function setPoidsGrain($biereId)
    {
        $ingredients = $this->entityManager->getRepository(Ingredient::class)->findBy([
            'biereId'   =>  $biereId,
            'is_brewed'  =>  Brassor::INGREDIENTS_PREVUS
        ]);
        $biere = $this->getBiereById($biereId);

        $poidsGrain = 0;
        for ($i = 0; $i < count($ingredients); $i++) {
            if ($ingredients[$i]->getTypeIngredient() == 'grain') {
                $poidsGrain += $ingredients[$i]->getQuantite();  // calcul du poids total du grain
            }
        }
        $biere->setPoidsGrain($poidsGrain); // indication du poids total du grain
    }

    

    // public function testCompletudeIngredient ($biereId)
    // {
    //     $variables = $this->typeIngredientsEssentiels;
    //     // dd($variables);
    //     $ingredients = $this->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS);
    //     $return['status'] = true;
    //     foreach($variables as $variable)
    //     {
    //         // dd($ingredients, $variable);
    //         $return[$variable] = true;
    //         if( count($ingredients[$variable]) == 0)
    //         {
    //             $return[$variable] = false;
    //             $return['status'] = false;
    //         }
    //     }
    //     return $return;
    // }

    // /**
    //  * Get the value of brassage
    //  */ 
    // public function getBrassage()
    // {
        
    //     return $this->biere;
    // }

    // /**
    //  * Set the value of brassage
    //  *
    //  * @return  self
    //  */ 
    // public function setBrassage($biere)
    // {
    //     $this->biere = $biere;

    //     return $this;
    // }

   
    // /**
    //  * Get the value of biere
    //  */ 
    // public function getBiere()
    // {
    //     return $this->biere;
    // }

    // /**
    //  * Set the value of biere
    //  *
    //  * @return  self
    //  */ 
    // public function setBiere($biere)
    // {
    //     $this->biere = $biere;

    //     return $this;
    // }

    public function brassageStepFwd($biereId)
    {

        $biere = $this->getBiereById($biereId);

        if ($biere->getState() < Biere::BRASSEE) 
        {
            if ($this->session->get('stateView') == $biere->getState()) {
                $biere->setState($biere->getState() + 1);
                $this->entityManager->flush();
            }
            $this->session->set('stateView', $this->session->get('stateView') + 1);
        }        
        else
        {
            dd('erreur étape');
        } 
        
    }

    public function brassageStepRwd($biereId)
    {
        $biere = $this->getBiereById($biereId);

        if ($biere->getState() > Biere::PROJET) 
        {
            if ($this->session->get('stateView') == $biere->getState())
            {
                $biere->setState($biere->getState() - 1);
                $this->entityManager->flush();
            }
            $this->session->set('stateView', $this->session->get('stateView') - 1);
        } 
        else
        {
            dd('erreur étape');
        }        
    }

    // /**
    //  * Get the value of transitions
    //  */ 
    // public function getTransitions()
    // {
    //     return $this->transitions;
    // }

    // /**
    //  * Set the value of transitions
    //  *
    //  * @return  self
    //  */ 
    // public function setTransitions($transitions)
    // {
    //     $this->transitions = $transitions;

    //     return $this;
    // }

    // /**
    //  * Get the value of etapes
    //  */ 
    // public function getEtapes()
    // {
    //     return $this->etapes;
    // }

    // /**
    //  * Set the value of etapes
    //  *
    //  * @return  self
    //  */ 
    // public function setEtapes($etapes)
    // {
    //     $this->etapes = $etapes;

    //     return $this;
    // }

    /**
     * Get the value of brassageParam
     */ 
    public function getBrassageParam()
    {
        return $this->brassageParam;
    }

    /**
     * Set the value of brassageParam
     *
     * @return  self
     */ 
    public function setBrassageParam($brassageParam)
    {
        $this->brassageParam = $brassageParam;
        return $this;
    }
    
    /**
     * affichageTypes
     *
     * @return void
     */
    public function affichageTypes()
    {        
        return $this->ingredientType;
    }

    /**
     * Get the value of ingredientType
     */ 
    public function getIngredientType()
    {
        return $this->ingredientType;
    }

    /**
     * Set the value of ingredientType
     *
     * @return  self
     */ 
    public function setIngredientType($ingredientType)
    {
        $this->ingredientType = $ingredientType;

        return $this;
    }

    /**
     * Get the value of typeIngredientsEssentiels
     */ 
    public function getTypeIngredientsEssentiels()
    {
        return $this->typeIngredientsEssentiels;
    }

    /**
     * Set the value of typeIngredientsEssentiels
     *
     * @return  self
     */ 
    public function setTypeIngredientsEssentiels()
    {
        $var = [];
        foreach($this->ingredientType as $type)
        {
            if(isset($type['essentiel']) && $type['essentiel'])
            {
                $var[] = $type['nom'];
            }

        }
        return $var;
    }

    /**
     * Get the value of messageButtons
     */ 
    public function getMessageButtons()
    {
        return $this->messageButtons;
    }

    /**
     * Set the value of messageButtons
     *
     * @return  self
     */ 
    public function setMessageButtons($messageButtons)
    {
        $this->messageButtons = $messageButtons;

        return $this;
    }
}
<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\Stock;
use App\Entity\TypeIngredient;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Object_;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class IngredientType extends GenericType
{
    private $entityManager;

    public function __construct (EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stock', EntityType::class, [
                'label' => ucfirst($options['ingredientType']['nom']),
//                'choice_label'  => 'Nom',
                'choice_value' => 'id',
                'class' => Stock::class,
                'choices' => $options['stocks']
            ])
            ->add('quantite', TextType::class,[
                'label'         =>  'Quantité en '. $options['ingredientType']['variables']['quantite']['unite'],
                'required'      =>  false,
                'constraints'   =>  new GreaterThanOrEqual(0)
            ]);
        if ($options['ingredientType']['nom'] == 'houblon' || $options['ingredientType']['nom'] == 'additif')
        {
            $builder
                ->add('tempsEbullition', TextType::class,[
                    'label'         =>  "temps d'ébullition en mn",
                    'required'      =>  false,
                    'constraints'   =>  new GreaterThanOrEqual(0)
                ]);
        }        
        $builder
            ->add('valider', SubmitType::class, [
                'label' =>  $options['buttonLabel'],
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success'
                ]
            ])
            ->add('quantiteInitiale', HiddenType::class, [
                'mapped'    =>  false,
                'attr'  =>  [
                    'value'   =>  $options['quantiteInitiale']
                ]
            ])            
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  Ingredient::class,
            'stocks'        =>  null,
            'buttonLabel'   =>  'Ajouter',
            'ingredientId'  =>  null,
            'ingredientType'    =>  null,
            'quantiteInitiale'  =>  null
        ]);
    }
}

//public function buildForm(FormBuilderInterface $builder, array $options)
//{
//    $ingredients = $options['ingredients'];
//    $builder
//        ->add('type', EntityType::class, [
//            'label' => 'Type de l\'ingredient:',
//            'choice_label' => 'Nom',
//            'choice_value'  =>  'id',
//            'class' =>  TypeIngredient::class,
////                'allow_extra_fields' => true,
//            'required' => false,
//            'placeholder'   =>  'Choisissez un type d\'ingrédient',
//            'attr' => [
//                'name' => 'typeOfIngredient'
//            ]
//        ]);
//    $formModifier = function(FormInterface $form, TypeIngredient $typeIngredient= null)
//    {
//        if($typeIngredient != null)
//        {
//            dd($typeIngredient);
//        }
//        $ingredients = null === $typeIngredient ?[]: $typeIngredient->getStocks() ;
//        $form->add('nom', EntityType::class,[
//            'class' =>  Stock::class,
//            'placeholder'   =>  '',
//            'choices'   =>  $ingredients
//        ]);
//    };
//
//    $builder->addEventListener(
//        FormEvents::PRE_SET_DATA,
//        function (FormEvent $event) use ($formModifier) {
//            $data = $event->getData();
//            $formModifier($event->getForm(), $data->getType());
//        });
//
//    $builder->get('type')->addEventListener(
//        FormEvents::POST_SUBMIT,
//        function(FormEvent $event) use ($formModifier) {
//            $data = $this->entityManager->getRepository(TypeIngredient::class)->findOneById($event->getForm()->getData());
//            $formModifier($event->getForm()->getParent(), $data);
//        }
//    );
//
//}



//                    if ($typeOfIngredientName == 'Malts') {
//                        $placeholder = 'en Kg';
//                    } else {
//                        $placeholder = 'en g';
//                    }
//                    $form
//
//                        ->add('quantite', TextType::class, [
//                            'label' => "Quelle quantité ?",
//                            'attr' => [
//                                'name' => 'quantite'
//                            ]
//                        ])
//                        ->add('idBiere', HiddenType::class, [
//                            'attr' => [
//                                'data' => $options['idBiere']
//                            ]
//                        ])
//                        ->add('submit', SubmitType::class, [
//                            'label' => 'Ajouter l\'ingrédient',
//                            'attr' => [
//                                'class' => self::BOOTSTRAP_CLASS_BUTTON
//                            ]
//                        ]);
//
//            });

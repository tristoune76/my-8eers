<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' =>  'Mon pseudo',
                'disabled'  =>  true
            ])
            ->add('oldPassword', PasswordType::class, [
                'label' =>  'Mon mot de passe actuel',
                'mapped'    =>  false,
                'attr'  =>  [
                    'placeholder'   =>  'Veuillez saisir votre mot de passe actuel'
                ]
            ])
            ->add('newPassword', RepeatedType::class, [
                    'type'  => PasswordType::class,
                    'invalid_message'   => 'les mots de passe doivent être identiques',
                    'label' =>  'Mon nouveau mot de passe',
                    'required'  => true,
                    'mapped'    =>  false,
                    'first_options'     => [
                        'label'    =>  'Saisissez votre nouveau mot de passe',
                        "attr"  => [
                            'placeholder' => 'Votre nouveau mot de passe'
                        ]
                    ],
                    'second_options'    => [
                        'label'    =>  'confirmez votre mot de passe',
                        "attr"  => [
                            'placeholder' => 'Votre nouveau mot de passe'
                        ]
                    ]
                ]
            )
            ->add('submit', SubmitType::class, [
                'label' => 'Volider'
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

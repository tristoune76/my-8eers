<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' =>  'Pseudo',                
                'attr'  =>  [
                    'value' =>  $options['lastUsername'],
                    'placeholder'   =>  'Votre pseudo',
                    'id'            =>  'inputUsername'
                ]
            ])
            ->add('password', PasswordType::class,[
                'label'     =>  'Mot de passe',
                'attr'      =>  [
                    'placeholder'   =>  'Votre mot de passe 2',
                    'id'        =>  'inputPassword'
                ]
            ])
            // ->add('_csrf_token', HiddenType::class,[
            //     'attr'  =>  [
            //         'value' =>  "{{ csrf_token('authenticate') }}"
            //     ]
            // ])
            ->add('submit', SubmitType::class, [
                'label' => 'Valider',
                'attr'  =>  [
                    'class'   =>  'btn btn-block btn-success mt-3'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'lastUsername'  =>  null
        ]);
    }
}


<?php

namespace App\Form;

use App\Entity\Palier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PalierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('temps', TextType::class, [

            ])
            ->add('temperature', TextType::class, [

            ])
            ->add('valider', SubmitType::class, [
                'label' =>  $options['buttonLabel'],
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success mx-1'
                ]
            ])
            ->add('annuler', SubmitType::class, [
                'label' =>  'Annuler',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success mx-1'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Palier::class,
            'buttonLabel'   =>  'Valider'

        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Biere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class IngredientStateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ingredientState', CheckboxType::class,[
                'label' =>  'Ingrédients validés',
                'required'  =>  false,
//                'attr'  =>  [
//                    'onclick'   =>   "checkIngredient()"
//                ]
            ])
//            ->add('submit', SubmitType::class,[
//                'label' =>  'Valider'
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Biere::class,
            'state'    =>  array([])
        ]);
    }
}

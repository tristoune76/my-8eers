<?php

namespace App\Form;

use App\Entity\Biere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class StateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('state', ChoiceType::class,[
//                'label'     =>  'Projet:',
//                'choices'    =>  $options['states'],
//                'multiple'  =>  false,
//                'expanded'  =>  true
//            ])
            ->add('state', CheckboxType::class,[
                'label'     =>  'Ingrédients confirmés:',
                'attr'      =>  [
                    'class' =>  'form-switch'
                ]
            ]);
//            ->add('submit', SubmitType::class,[
//                'label' =>  'Valider',
//                'attr'  =>  [
//                    'class'   =>  'btn btn-outline-success'
//                ]
//                
//            ])
//        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => Biere::class,
            'states'        =>  array([])
        ]);
    }
}

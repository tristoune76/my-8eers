<?php

namespace App\Form;

use App\Entity\Biere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class VolPrevuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $placeholder = $options['placeholder'];
        $builder
            ->add('volFinalPrevu',  TextType::class, [
                'label'         =>  'Quel volume final de bière?',
                'constraints'   => new GreaterThanOrEqual(0),
                'attr'          =>  [
                    'placeholder'   =>  $placeholder
                ]
            ])
            // ->add('annuler', SubmitType::class,[
            //         'label' =>  'Annuler',
            //         'attr'  =>  [
            //             'class'   =>  'btn btn-outline-success mx-1'
            //         ]
            //     ]
            // )
            ->add('valider', SubmitType::class, [
                'label' =>  'Valider',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success mx-1'
                    ]
                ]
            )            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    =>  Biere::class,
            'placeholder'   =>  ''
        ]);
    }
}

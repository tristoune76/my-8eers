<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuantiteType extends GenericType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('toZero', SubmitType::class, [
                'label' =>  'Remettre à zéro',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success'
                ]
            ])
            ->add('quantityUp', SubmitType::class, [
                'label' =>  'Rajouter',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success btn-sm'
                ]
            ])
            ->add('quantityDown', SubmitType::class, [
                'label' =>  'Enlever',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success btn-sm'
                ]
            ])
            ->add('quantity',TextType::class, [
                'label'     =>  false,
                'required'  =>  false
//                'attr'  =>  [
//                    'class'   =>  ''
//                ]
            ])
            ->add('ingredientId', HiddenType::class,[
                'label'     =>  false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\TypeBiere;
use App\Entity\TypeIngredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeBiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, [
                'label' =>  false,
                'required' => false,
                'attr'  =>  [
                    'placeholder'   =>  'Le type de la bière'
                ]
            ])
            ->add('valider', SubmitType::class, [
                'label' =>  $options['buttonLabel'],
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success'
                ]
            ])
            ->add('annuler', SubmitType::class, [
                'label' =>  'Annuler',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success',
                    'href' => "{{ path('indexTypeBiere') }}", 
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TypeBiere::class,
            'buttonLabel'   =>  'Valider'
        ]);
    }
}

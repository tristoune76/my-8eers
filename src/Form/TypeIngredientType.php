<?php

namespace App\Form;

use App\Entity\TypeBiere;
use App\Entity\TypeIngredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeIngredientType extends GenericType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, [
                'label' =>  false,
                'attr'  =>  [
                    'placeholder'   =>  'Le type de l\'ingredient'
                ]
            ])
            ->add('valider', SubmitType::class, [
                'label' =>  $options['buttonLabel'],
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success'
                ]
            ])
            ->add('annuler', SubmitType::class, [
                'label' =>  'Annuler',
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success'
                ]
            ]);
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        Parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => TypeIngredient::class,
            'buttonLabel'   => 'Valider'
        ]);
    }
}

<?php

namespace App\Form;

use ArrayAccess;
use App\Entity\Biere;
use App\Entity\TypeBiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BiereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //dd($options['typesBiere']);
        // foreach ($options['typesBiere'] as $typeBiere)
        // {
        //     $typesBiere[$typeBiere->getNom()] = $typeBiere->getId() ;
        // }
        //dd($typesBiere);
        $builder
            ->add('nom',TextType::class, [
            // 'label' =>  'Quel est le nom de votre bière?',
                'required' => false,    
                'attr'  =>  [
                    'placeholder'   =>  'Nom de la bière'
                ]
            ])
            ->add('type', EntityType::class, [
                // 'label' =>  'Quel est le type de votre bière?',
                'required' => false,
                'class' => TypeBiere::class,
                'multiple' => false,
                'attr'  =>  [
                    'placeholder'   =>  'Type de la biere'
                ]
            ])
            ->add('description',TextareaType::class, [
            // 'label' =>  'Décrivez votre bière?',
                'required' => false,
                'attr'  =>  [
                    'placeholder'   =>  'Description de la bière'
                ]
            ])
            ->add('annuler', SubmitType::class,
                [
                    'label' =>  'Annuler',
                    // 'validation_groups' => false,
                    'attr'  =>  [
                        'class'   =>  'btn btn-outline-success mx-1'
                    ]
                ]
            )
            ->add('valider', SubmitType::class, [
                'label' =>  $options['buttonLabel'],
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success mx-1'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Biere::class,
            // 'typesBiere'  =>  array([]),
            'buttonLabel'   =>  'Valider'
        ]);
    }
}

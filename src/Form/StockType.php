<?php

namespace App\Form;

use App\Entity\Stock;
use App\Entity\Levures;
use App\Form\GenericType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class StockType extends GenericType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $unite = 'en g';
        $builder
            ->add('nom',TextType::class, [
                'label' =>  'Son nom:',
                'attr'  =>  [
                    'placeholder'   =>  'Nom'
                ]
            ])
        ;

        if($options['ingredientType'] == 'houblon')
        {
            $builder
                ->add('type', ChoiceType::class, [
                    'label' =>  'Son type:',
                    'choices'   =>  [
                        'aromatique'                =>  'aromatique',
                        'amérisant'                 =>  'amérisant',
                        'aromatique et amérisant'   =>  'aromatique et amérisant'
                    ]                        
                ])                
                ->add('aromes',TextType::class, [
                    'label'     =>  'Ses arômes:',
                    'attr'  =>  [
                        'placeholder'   =>  'arômes'
                    ]                    
                ])
                ->add('acidesAlphaMin',TextType::class, [
                'label' =>  'Son taux d\'acide alpha min:',
                'attr'  =>  [
                    'placeholder'   =>  'taux d\'acides alpha min'
                    ]
                ])
                ->add('acidesAlphaMax',TextType::class, [
                    'label' =>  'Son taux d\'acide alpha max:',
                    'attr'  =>  [
                        'placeholder'   =>  'taux d\'acides alpha max'
                    ]
                ])
            ;
        }
        elseif($options['ingredientType'] == 'grain')
        {
            $unite = 'en Kg';
            $builder
                ->add('EBCMin',TextType::class, [
                    'label' =>  'Son EBC min:',
                    'attr'  =>  [
                        'placeholder'   =>  'EBC Min'
                    ]
                ])
                ->add('EBCMax',TextType::class, [
                    'label' =>  'Son EBC max:',
                    'attr'  =>  [
                        'placeholder'   =>  'EBC Max'
                    ]
                ])
            ;

        }
        elseif($options['ingredientType'] == 'levure')
        {
            $builder
                ->add('type', ChoiceType::class, [
                    'label' =>  'Son type:',
                    'choices'   =>  [
                        'liquide'   =>  'liquide',
                        'sèche'     =>  'sèche'
                    ]                        
                ]);     
        } 
        elseif ($options['ingredientType'] == 'additif') 
        {
            $builder
                ->add('description', TextareaType::class, [
                    'label' =>  'Sa description:',
                    'required' => false,
                    'attr'  =>  [
                        'placeholder'   =>  "Description de l'additif"
                    ]
                ]);
        }
        $builder
            ->add('quantite',TextType::class, [
                'label' =>  'Quantité',
                'required'      =>  false,
                'attr'  =>  [
                    'placeholder'   =>  $unite
                ]
            ])
            // ->add('annuler',SubmitType::class, [
            //     'label' =>  'Annuler',
            //     'attr'  =>  [
            //         'class'   =>  'btn btn-outline-success mx-1'
            //     ]
            // ])
            ->add('valider', SubmitType::class, [
                'label' =>  $options['buttonLabel'],
                'attr'  =>  [
                    'class'   =>  'btn btn-outline-success mx-1'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        Parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => Stock::class,
            'ingredientType'    =>  null
        ]);
    }
}
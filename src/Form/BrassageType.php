<?php

namespace App\Form;

use Exception;
use App\Entity\Biere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BrassageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        try
        {
            $builder
                ->add($options['field'])
                ->add('valider', SubmitType::class, [
                    'label' =>  $options['buttonLabel'],
                    'attr'  =>  [
                        'class'   =>  'btn btn-outline-success mx-1'
                    ]
                ])
                ->add('annuler', SubmitType::class, [
                    'label' =>  'Annuler',
                    'attr'  =>  [
                        'class'   =>  'btn btn-outline-success mx-1'
                    ]
                ]);
        }
        catch(Exception $e)
        {
            dd($e);
        }
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Biere::class,
            'buttonLabel'   =>  'Valider',
            'field'         =>  ''
        ]);
    }
}

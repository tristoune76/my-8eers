<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' =>  'Votre pseudonyme',
                'attr'  =>  [
                    'placeholder'   =>  'Entrez votre pseudonyme'
                ]
            ])
            ->add(
                'password',
                RepeatedType::class,
                [
                    'type'  => PasswordType::class,
                    'invalid_message'   => 'les mots de passe doivent être identiques',
                    'label' =>  'Votre mot de passe',
                    'required'  => true,
                    'first_options'     => [
                        'label'    =>  'Votre mot de passe',
                        "attr"  => [
                            'placeholder' => 'merci de saisir votre mot de passe'
                        ]
                    ],
                    'second_options'    => [
                        'label'    =>  'confirmez votre mot de passe',
                        "attr"  => [
                            'placeholder' => 'merci de confirmer votre mot de passe'
                        ]
                    ]
                ]
            )
            ->add('submit', SubmitType::class, [
                'label' => 's\'inscrire'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Levures;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenericType extends AbstractType
{
    const BOOTSTRAP_CLASS_BUTTON = 'btn btn-block btn-success m-2';
    CONST BUTTON_DEFAULT_TEXT = 'Valider';
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'buttonLabel'   =>  self::BUTTON_DEFAULT_TEXT
        ]);
    }
}
<?php

namespace App\Controller;

use DateTime;
use Exception;
use App\Lib\Brassor;
use App\Lib\Message;
use App\Entity\Biere;
use App\Entity\Palier;
use App\Lib\Validator;
use App\Form\BiereType;
use App\Lib\Utilitaire;
use App\Form\PalierType;
use App\Entity\TypeBiere;
use App\Entity\Ebullition;
use App\Entity\Ingredient;
use App\Form\BrassageType;
use App\Form\TypeBiereType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

class BiereController extends AbstractController
{
    private $entityManager;
    private $session;
    private $brassor;
    private $validator;
    private $utilitaire;


    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Brassor $brassor, Validator $validator, Utilitaire $utilitaire)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->brassor = $brassor;
        $this->validator = $validator;
        $this->utilitaire = $utilitaire;
    }
    
    
    /**
     * @Route("/biere", name="indexBiere")
     */
    public function index(): Response
    {
        
        /** TODO récupérer par date */
        $message = $this->session->remove('message');
        $bieresBrassees = [];
        $bieresEnCours = [];
        $bieresEnProjet = [];
        $bieres = $this->entityManager->getRepository(Biere::class)->findAll();
        foreach ($bieres as $biere)
        {
            if($biere->getState() >= Biere::BRASSEE)
            {
                $bieresBrassees[] = $biere;
            }
            else if ($biere->getState() <= Biere::PRETE)
            {
                $bieresEnProjet[] = $biere;
            }
            else
            {
                $bieresEnCours[] = $biere;
            }
        }
        return $this->render('biere/biere.html.twig', [
            'bieresEnProjet'    =>  $bieresEnProjet,
            'bieresEnCours'     =>  $bieresEnCours,
            'bieresBrassees'    =>  $bieresBrassees,
            'message'           =>  $message,
        ]);
    }

    /**
     * @Route("/biere/add", name="addBiere")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $biere = new Biere();
        return $this->saveBiere($request, $biere);
    }
    
    /**
     * @Route("/biere/modify/{id}", name="modifyBiere")
     * @param Request $request
     * $param $id
     * @return Response
     */
    public function modify (Request $request, $id)
    {
        $biere = $this->brassor->getBiereById($id);
       
        return  $this->saveBiere($request, $biere);        
    }

    /**
     * @Route("/biere/remove/{id}", name="removeBiere")
     * @param $id
     * @return Response
     */
    public function removeBiere($id): Response
    {
        $biere = $this->brassor->getBiereById($id);

        try {
            $this->entityManager->remove($biere);
            $this->entityManager->flush();
            $notification = "Suppression de la bière '" . $biere->getNom() . "' réussie.";
            $status = 'success';
            
        } catch (ForeignKeyConstraintViolationException $e) {
            $notification = "Vous ne pouvez pas supprimer la bière '" . $biere->getNom() . "'. Des ingrédients lui sont affectés";
            $status = 'danger';
            
            // $this->session->add('errors', $e);
        }
        $this->utilitaire->notify($notification, $status);
        return $this->redirectToRoute('indexbiere');
    }

   

    /**
     * @Route("/biere/{biereId}/brassage/validate", name="brassageValidate")
     * @param $biereId
     * @param Request $request
     * @return Response
     */
    public function brassageValidate($biereId)
    {
        // $stateView = $_GET['stateView'];
        $stateView = $this->session->get('stateView');
        $biere = $this->brassor->getBiereById($biereId);
        $validationMessage = $this->validator->stateValidator($stateView, $biere);
        if ($validationMessage == '') 
        {
            return $this->redirectToRoute('brassageFwd', [
                'biereId'   =>  $biereId,
                // 'stateView' =>  $stateView
            ]);
        }
        else
        {
            
            $notification =  $validationMessage;
            $status = 'danger';
            $buttonMessage = 'Continuer sans saisir';
            $button = [                
                [
                    'path'      => 'brassageFwd',
                    'variables' =>  [
                        'biereId'       =>  $biere->getId(),
                    ],
                    'titre'     =>  $buttonMessage,
                    'class'     =>  'text-wrap'
                ]
            ];
            
            $this->utilitaire->notify($notification, $status, $button);
            return $this->redirectToRoute('brassageEtape', [
                'biereId'   =>  $biereId,
            ]);
        }
    }


    /**
     * @Route("/biere/{biereId}/brassage/forward", name="brassageFwd")
     * @param $biereId
     * @param Request $request
     * @return Response
     */
    public function brassageFwd($biereId)
    {
        // $stateView = $_GET['stateView'];

        $this->brassor->brassageStepFwd($biereId);
             

        return $this->redirectToRoute('brassageEtape', [
            'biereId'   =>  $biereId,
        ]);
    }

    /**
     * @Route("/biere/{biereId}/brassage/rewind", name="brassageRwd")
     * @param $biereId
     * @param Request $request
     * @return Response
     */
    public function brassageRwd($biereId)
    {
        $this->brassor->brassageStepRwd($biereId);

        return $this->redirectToRoute('brassageEtape', [
            'biereId'   =>  $biereId,
        ]);
    }

    /**
     * @Route("/biere/{biereId}/brassage/continue", name="brassageContinue")
     * @param $biereId
     * @param Request $request
     * @return Response
     */
    public function brassageContinue($biereId)
    {
        if(isset($_GET['stateView']))
        {
            $this->session->remove('stateView');
            $this->session->set('stateView', $_GET['stateView']);
        }
        

        return $this->redirectToRoute('brassageEtape', [
            'biereId'   =>  $biereId,
        ]);
    }

    /**
     * @Route("/biere/{biereId}/brassage/etape", name="brassageEtape")
     * @param $biereId
     * @param $stateView
     * @return Response
     */
    public function brassageEtape($biereId, $form = null, $parametre = null)
    {
        try{

            $biere = $this->brassor->getBiereById($biereId);
            $message = $this->session->remove('message'); 
            // dd($this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS));

            $parametres = [
                'biere'             =>  $biere,
                'ingredientsPrevus' =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS),
                'ingredientsReels'  =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS),
                'paliers'           =>  $this->brassor->getPaliersByIdBiere($biereId),
                'headers'           =>  $this->brassor->getIngredientType(),
                'brassageParam'     =>  $this->brassor->getBrassageParam(),
                'message'           =>  $message,
            ];
            if ($form)
            {
                $parametres['form'] =  $form->createView();  
            }
            if ($parametre)
            {
                $parametres['parametre'] = $parametre;  

            }
            // if ($message && $message['status'] != "success")
            // {
            //     $parametres['notificationButtons'] = $buttons;
            // }
            // if ($message)
            // {
            //     $parametres['message'] = $message;
            // }
            return $this->render('biere/brassage.html.twig', $parametres);

        }
        catch (Exception $e)
        {   
            dd($e);
        }
        

        
    }

    /**
     * @Route("/biere/{biereId}/brassage/stop", name="brassageStop")
     * @param $biereId
     * @param $stateView
     * @return Response
     */
    public function brassageStop($biereId)
    {
        $ingredients = $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS);
        $this->session->set('stateView', Biere::STOP);

        return $this->brassageEtape($biereId);

        // return $this->render('biere/brassage.html.twig', [
        //     'ingredients'       =>  $ingredients,
        //     'headers'           =>  $this->brassor->getIngredientType(),
        //     'biere'             =>  $this->brassor->getBiereById($biereId),
        //     'brassageParam'     =>  $this->brassor->getBrassageParam()

        // ]);

    }

    /**
     * @Route("/biere/{biereId}/brassage/parametres/{parametre}", name="parametre")
     * @param $biereId
     * @param $parametre
     * @return Response
     */
    public function parametre($biereId, $parametre, Request $request)
    {
        $biere = $this->brassor->getBiereById($biereId);
        
        // $this->session->remove('stateView');
        // $stateView = $_GET['stateView'];
        // $this->session->set('stateView', $stateView);

        
        $form = $this->createForm(BrassageType::class, $biere, [
            'buttonLabel'   =>    'valider',
            'field'         =>   $parametre
        ]);

           
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if ($form->get('valider')->isClicked()) // si on appuie sur le bouton 'Valider
            {
                try 
                {
                    $this->entityManager->flush();
                } 
                catch (Exception $e) 
                {
                    dd($e);
                }
            }
            return $this->redirectToRoute('brassageEtape', [
                'biereId'   =>  $biereId
                ]);
        }

        return $this->brassageEtape($biereId, $form, $parametre);

        // return $this->render('biere/brassage.html.twig', [
        //     'biere'             =>  $biere,
        //     'ingredients'       =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS),
        //     'headers'           =>  $this->brassor->getIngredientType(),
        //     'paliers'           =>  $this->brassor->getPaliersByIdBiere($biereId),
        //     'form'              =>  $form->createView(),
        //     'brassageParam'     =>  $this->brassor->getBrassageParam(),
        //     'parametre'         =>  $parametre,

        // ]);

    }

    /**
     * @Route("/biere/{biereId}/brassage/parametres/palier/add", name="palierAdd")
     * @param $biereId
     * @return Response
     */

    public function palieradd($biereId, Request $request)
    {
        $palier = new Palier();
        return $this->savePalier($biereId, $palier, $request);
    }

    /**
     * @Route("/biere/{biereId}/brassage/parametres/palier/modify/{palierId}", name="palierModify")
     * @param $biereId
     * @param $palierId
     * @return Response
     */

    public function palierModify($biereId, $palierId, Request $request)
    {
        $palier = $this->entityManager->getRepository(Palier::class)->findOneBy([
            'id'    =>  $palierId
            ]);
        return $this->savePalier($biereId, $palier, $request);
    }

    /**
     * @Route("/biere/{biereId}/brassage/parametres/palier/remove/{palierId}", name="palierRemove")
     * @param $biereId
     * @param $palierId
     * @return Response
     */

    public function palierRemove($biereId, $palierId, Request $request)
    {
    }

    public function savePalier($biereId, $palier, $request)
    {
        $biere = $this->brassor->getBiereById($biereId);

        // $this->session->remove('stateView');
        // $stateView = $_GET['stateView'];
        // $this->session->set('stateView', $stateView);


        $form = $this->createForm(PalierType::class, $palier, [
            'buttonLabel'   =>    'valider',
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('valider')->isClicked()) // si on appuie sur le bouton 'Valider
            {
                try 
                {
                    if (!$palier->getId()) 
                    {
                        $palier->setBiere($biere);
                        $this->entityManager->persist($palier);
                    }
                    $this->entityManager->flush();
                } 
                catch (Exception $e) 
                {
                    dd($e);
                }
            }
            return $this->redirectToRoute('brassageEtape', ['biereId'   =>  $biereId]);
        }
        return $this->brassageEtape($biereId, $form, "palier");

        // return $this->render('biere/brassage.html.twig', [
        //     'biere'             =>  $biere,
        //     'ingredients'       =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS),
        //     'typesOfIngredient' =>  $this->typesOfIngredient,
        //     'paliers'           =>  $this->brassor->getPaliersByIdBiere($biereId),
        //     'form'              =>  $form->createView(),
        //     'parametre'         =>  "palier"

        // ]);
    }

    public function saveBiere(Request $request, $biere)
    {
        $bieres = $this->entityManager->getRepository(Biere::class)->findAll();
        $bieresBrassees = [];
        $bieresEnCours = [];
        $bieresEnProjet = [];
        foreach ($bieres as $item) {
            if ($item->getState() >= Biere::BRASSEE) {
                $bieresBrassees[] = $item;
            } else if ($item->getState() <= Biere::PRETE) {
                $bieresEnProjet[] = $item;
            } else {
                $bieresEnCours[] = $item;
            }
        }

        $form = $this->createForm(BiereType::class, $biere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('valider')->isClicked()) // si on appuie sur le bouton 'Valider
            {
                try {
                    if (!$biere->getId()) {
                        $biere->setState(Biere::PROJET);
                        $biere->setDateAjout(new \DateTime('now'));
                        $biere->setUser($this->getUser());
                        // $biere->setIngredientState(0);
                        $biere->setTpsEmpatage(0);
                        $this->entityManager->persist($biere);
                        $notification = "La bière '" . $biere->getNom() . "' a été ajoutée";
                    } else
                    {
                        $notification = "La bière '" . $biere->getNom() . "' a été modifiée";
                    }
                    $this->entityManager->flush();

                    $status = 'success';
                } 
                catch (Exception $e) 
                {
                    $notification = "La bière n'a pas été ajoutée/modifiée. Au moins un des champs est vide";
                    $status = 'danger';
                    dd($e);
                    // $this->session->add('errors', $e);
                }
                $this->utilitaire->notify($notification, $status);
                return $this->redirectToRoute('indexBiere');
            }
            elseif ($form->get('annuler')->isClicked()) // si on appuie sur le bouton 'Annuler'
            {
                return $this->index();
            }
        }
        return $this->render('biere/biere.html.twig', [
            'form'              =>  $form->createView(),
            'bieresEnProjet'    =>  $bieresEnProjet,
            'bieresEnCours'     =>  $bieresEnCours,
            'bieresBrassees'    =>  $bieresBrassees,
            'biere'             =>  $biere
        ]);
    }
}

// /**
//      * @Route("/biere/{biereId}/empatage/paliers/add", name="addPalier")
//      * @param $biereId
//      * @param Request $request
//      * @return Response
//      */
//     public function addPalier($biereId, Request $request)
//     {
//         $palier = new Palier();
//         return $this->brassoy->savePalier($biereId, $palier, $request);
//     }

//     /**
//      * @Route("/biere/{biereId}/empatage/paliers/modify/{palierId}", name="modifyPalier")
//      * @param $biereId
//      * @param $palierId
//      * @param Request $request
//      * @return Response
//      */
//     public function modifyPalier($biereId, $palierId, Request $request)
//     {
        
//         $palier = $this->entityManager->getRepository(Palier::class)->findOneBy([
//             'id'    =>  $palierId
//         ]);
//         return $this->brassor->savePalier($biereId, $palier, $request);
//     }

    
//     public function savePalier($biereId, $palier, Request $request)
//     {
//         $biere = $this->brassor->getBiereById($biereId);

//         $paliers = $this->entityManager->getRepository(Palier::class)->findByBiere([
//             'biere'  => $biere
//         ]);

//         $form = $this->createForm(PalierType::class, $palier);
//         $form->handleRequest($request);

//         if ($form->isSubmitted() && $form->isValid()) 
//         {
//             if (!$palier->getId())
//             {
//                 $palier->setBiere($biere);
//                 $this->entityManager->persist($palier);
//             }            
//             $this->entityManager->flush();
//             $biere->setTpsEmpatage($biere->getTpsEmpatage()+$palier->getTemps());
//             return $this->redirectToRoute('resumeEmpatage', [
//                 'paliers'   =>  $paliers,
//                 'biere'  =>  $biere
//             ]);
//         }
//         return $this->render('biere/empatageResume.html.twig', [
//             'form'  =>  $form,
//             'biere'  =>  $biere
//         ]);  
//     }

//     /**
//      * @Route("/biere/{biereId}/empatage/paliers/remove/{palierId}", name="removePalier")
//      * @param $biereId
//      * @param $palierId
//      * @return Response
//      */
//     public function removePalier($biereId, $palierId)
//     {
//         $palier = $this->entityManager->getRepository(Palier::class)->findOneBy([
//             'id'    =>  $palierId
//         ]);
//         $biere = $this->brassor->getBiereById($biereId);

//         $this->brassor->removePalier($palier, $biere);
        
//         return $this->redirectToRoute('resumeEmpatage', [
//             'paliers'   =>  $this->brassor->getPaliersByIdBiere($biereId),
//             'biere'  =>  $biere
//         ]);
//     }
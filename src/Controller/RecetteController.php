<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Lib\Utility;

class RecetteController extends AbstractController
{
    /**
     * @Route("/recette", name="recette")
     */
    public function index($idBiere): Response
    {
        $biere = getBiereById($id);
        return $this->render('recette/index.html.twig', [
            'biere' => $biere,
        ]);
    }
}

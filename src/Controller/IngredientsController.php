<?php

namespace App\Controller;

use App\Lib\Brassor;
use App\Lib\Utility;
use App\Entity\Biere;
use App\Entity\Stock;
use App\Lib\Validator;
use App\Entity\Brassage;
use App\Entity\Ebullition;
use App\Entity\Ingredient;
use App\Form\VolPrevuType;
use App\Form\IngredientType;
use App\Lib\TypeOfIngredient;
use App\Entity\TypeIngredient;
use App\Form\IngredientStateType;
use App\Lib\Stockator;
use App\Lib\Utilitaire;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Notifier\Texter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IngredientsController extends AbstractController
{
    
    
    private $entityManager;
    private $brassor;
    private $session;
    private $validator;
    private $stockator;
    private $utilitaire;
    private $biereController;

    public function __construct(
        EntityManagerInterface $entityManager,
        Brassor $brassor,
        SessionInterface $session,
        Validator $validator,
        Stockator $stockator,
        Utilitaire $utilitaire,
        BiereController $biereController)
    {
        $this->entityManager = $entityManager;
        $this->brassor = $brassor;
        $this->session = $session;
        $this->validator = $validator;
        $this->stockator = $stockator;
        $this->utilitaire = $utilitaire;
        $this->biereController = $biereController;
    }

    /**
     * @Route("/biere/ingredients/index/{biereId}", name="indexIngredients")
     * @param $biereId
     * @return Response
     */
    public function index($biereId): Response
    {
        $message = $this->session->remove('message');
        
        $biere = $this->brassor->getBiereById($biereId);             
         
        $ingredients = $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS);

        return $this->render('ingredients/index.html.twig', [
            'ingredients'   =>  $ingredients,
            'biere'         =>  $biere,
            'message'       =>  $message,
            'headers'       =>  $this->brassor->getIngredientType(),
            'ingredient'    =>  null
        ]);
        // }
    }

    /**
     * @Route("/biere/ingredients/save/{biereId}/{ingredientType}", name="addIngredient")
     * @param Request $request
     * @param $biereId
     * @param  $ingredientType
     * @return Response
     */
    public function add(Request $request, $biereId,  $ingredientType): Response
    {
        $ingredient = new Ingredient;
        
        $ingredient =  $this->newIngredient($ingredient, $biereId, $ingredientType);      
       
        return $this->processFormSave($request, $ingredient);        
    }

    /**
     * @Route("/biere/ingredients/modify/{id}", name="modifyIngredient")
     * @param Request $request
     * @param null $ingredientId
     * @return Response
     */
    public function modify(Request $request,  $id): Response
    {
        $ingredient = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
            'id'    =>  $id
        ]);
        return $this->processFormSave($request, $ingredient, $ingredient->getQuantite());
    }
  
    /**
     * @Route("/biere/ingredients/remove/{id}", name="removeIngredient")
     * @param $id
     * @return Response
     */
    public function remove( $id): Response
    {
        $ingredient = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
            'id'    =>  $id
        ]);
        $stock = $ingredient->getStock();
        if ($ingredient->getIsBrewed()) // Suppression d'un ingrédient durant le brassage. Il donc déjà été retiré du stock. Il faut le remettre.
        {
            $unite = $this->brassor->getIngredientType()[$stock->getTypeIngredient()]['variables']['quantite']['unite'];
            $nouvelleQuantite = $ingredient->getQuantite() + $stock->getQuantite();

            $stock->setQuantite($nouvelleQuantite );
            $notification = "Le stock de " . $stock->getNom() . " a été reapprovisionné de " . $ingredient->getQuantite() . $unite . ". Il reste " .$nouvelleQuantite  . $unite . ".\n";
            $status = "success";

        }
        else    // suppression d'un ingrédient durant la conception de la recette. Il n'a donc pas encore été retiré du stock
        {
            $notification = "La/Le ". $stock->getNom()." a été supprimé(e) de la liste des ingrédients"."\n";
            $status = "success";
        }

        $this->utilitaire->notify($notification,$status);

        $this->entityManager->remove($ingredient);
        $this->entityManager->flush();

        return $this->redirectToRoute('brassageContinue', [
            'biereId'   =>  $ingredient->getBiereId(),
            // 'stateView' =>  0
        ]);
    }

    /**
     * @Route("/biere/ingredients/retrieveFromStock/{biereId}", name="retrieveFromStock")
     * @param $biereId
     * @return 
     *
     **/
    public function retrieveFromStock($biereId)
    {
        /**
         * Retrait des ingrédients:
         * - Si retour true => retrait avec succes, pas de problème de stock
         * - Si retour false => échec du retrait. Il doit manquer du stock
         */

        if ($this->stockator->retrieveFromStock($biereId))
        {
            return $this->redirectToRoute('brassageFwd', [
                'biereId'   =>  $biereId
            ]);
        }
        else
        {
            return $this->redirectToRoute('brassageEtape', [
                'biereId'   =>  $biereId,
                'noButton'  =>  true
            ]);
        }

        
        
    }

    /**
     * @Route("/biere/ingredients/replaceInStock/{biereId}", name="replaceInStock")
     * @param $biereId
     * @return 
     * Cette fonction est appelée par le script javascript déclenché par le switch sur la page des ingrédients. 
     * Il permet soit de retirer les quantités du stock (action on du switch), soit de les remettre (action off pour modifier du switch).
     */
    public function replaceInStock($biereId)
    {
        $this->stockator->replaceInStock($biereId);
        return $this->redirectToRoute('brassageRwd', [
            'biereId'   =>  $biereId
        ]);
    }

    /**
     * @Route("/biere/{biereId}/VolPrevu", name="volFinalPrevu")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function volFinalPrevu($biereId, Request $request): Response
    {
        $biere = $this->brassor->getBiereById($biereId);

        $form = $this->createForm(VolPrevuType::class, $biere, [
                'placeholder'   =>  'en litre'
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
            return $this->redirectToRoute('brassageContinue', [
                    'biereId'   =>  $biereId,
                    // 'stateView' =>  0
                ]);
        }

        return $this->render('ingredients/index.html.twig', [
            'ingredients'   =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS),
            'biere'         =>  $biere,
            'headers'       =>  $this->brassor->getIngredientType(),
            'param'         =>  'volFinalPrevu',
            'form'          =>  $form->createView()
        ]);        
    }

    public function processFormSave(Request $request, $ingredient, $quantitéInitiale = 0)
    {
        $biereId = $ingredient->getBiereId();
        $biere = $this->brassor->getBiereById($biereId);
        // $this->session->remove('stateView');
        // $stateView = $_GET['stateView'];
        $stateView = $this->session->get('stateView');
        // dd($stateView);
        $this->session->set('stateView', $stateView);
        $stocks = $this->stockator->isStockForIngredient($ingredient);

        if (!$ingredient->getId())  //Si c'est un nouvel ingrédient dans la recette alors on propose les choix dans le formulaire
        {
            if (!$stocks) // S'il n'y a pas d'ingrédient du type demandé dans les stocks alors on l'indique
            {               
                $this->session->remove('stateview');
                $this->session->set('stateView', 0);
                return $this->redirectToRoute('brassageContinue', [
                    'biereId'   =>  $biereId,
                ]);
            }            
        } 
        $form = $this->createForm(IngredientType::class, $ingredient, [
            'stocks'            =>  $stocks,
            'ingredientType'    =>  $this->brassor->getIngredientType()[$ingredient->getTypeIngredient()],
            'quantiteInitiale'  =>  $ingredient->getQuantite()
        ]);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $ingredientExistant = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
                'stock'    =>   $ingredient->getStock(),
                'biereId'    =>   $ingredient->getBiereId(),
                'is_brewed'  =>  $ingredient->getIsBrewed()
            ]);
            if ($ingredient->getIsBrewed() == Brassor::INGREDIENTS_REELS)   //En cours de brassage => retrait du stock immédiat
            {
                if (!$ingredient->getId())
                {
                    $quantiteInitiale = 0;
                    if ($ingredientExistant)
                    {  // S'il y a  un ingrédient du même type dans la liste d'ingrédient
                        if ($ingredientExistant->getTempsEbullition() and $ingredientExistant->getTempsEbullition() == $ingredient->getTempsEbullition()
                            or
                            !$ingredientExistant->getTempsEbullition()) //Si un temps d'ébullition est précisé
                        {                           
                            // Retrait dans les stock (modifié ou additionné)
                            $quantiteInitiale = $ingredientExistant->getQuantite();
                            $ingredient = $ingredientExistant;
                            $nouvelleQuantite = $form->get('quantite')->getData() + $ingredient->getQuantite();
                            // dd($nouvelleQuantite);
                            $ingredient->setQuantite($nouvelleQuantite);                                                        
                        }
                    }                    
                }
                else
                {
                    $quantiteInitiale = $form->get('quantiteInitiale')->getData();
                }

                // Retrait dans les stock (modifié ou additionné)
                $retour = $this->stockator->retrieveOneIngredientFromStock($ingredient, $quantiteInitiale);

                if ($retour['status'] == 'success') {
                    $this->entityManager->flush(); // s'il y a assez de stock
                }
                $this->utilitaire->notify($retour['notification'], $retour['status']);
            }
            else    //Avant le brassage => pas de retrait du stock encore. On l'inscrit dans la liste des ingrédients
            {
                if (!$ingredient->getId())  // Nouvel ingrédient
                {
                    // vérification que l'ingrédient n'existe pas
                    
                    // dd ($ingredientExistant);
                    if (!$ingredientExistant)  // S'il n'y a pas d'ingrédient du même type dans la liste d'ingrédient
                    {
                        $this->entityManager->persist($ingredient);
                    }
                    else    //S'il y a déjà un ingrédient du même type dans a liste
                    {
                        if ($ingredientExistant->getTempsEbullition()) //Si un temps d'ébullition est précisé
                        {
                            if ($ingredientExistant->getTempsEbullition() == $ingredient->getTempsEbullition())
                            {
                                $ingredientExistant->setQuantite($ingredient->getQuantite());
                            }
                            else    //Si les temps d'ébullitions précisés ne sont pas les mêmes alors c'est une nouvelle entrée dans la liste
                            {
                                $this->entityManager->persist($ingredient);
                            }
                        } 
                        else //S'il n'y a pas de temps débullition de précisé dans ce cas on met à jour la quantité de l'ingrédient trouvé
                        {
                            $ingredientExistant->setQuantite($ingredient->getQuantite());
                        }

                    }
                }
                $this->entityManager->flush();

            }
            return $this->redirectToRoute('brassageEtape', [
                'biereId'   =>  $biereId,
                'nobutton'  =>  true
            ]);
        }
        // } else {
        //     if ($ingredient->getId()) {
        //         $ingredientSave = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
        //             'id'    =>  $ingredient->getId()
        //         ]);
        //     }
        // }

        return $this->render('biere/brassage.html.twig', [
            'biere'             =>  $biere,
            'ingredientsReels'  =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS),
            'ingredientsPrevus' =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_PREVUS),
            'headers'           =>  $this->brassor->getIngredientType(),
            'paliers'           =>  $this->brassor->getPaliersByIdBiere($biereId),
            'form'              =>  $form->createView(),
            'brassageParam'     =>  $this->brassor->getBrassageParam(),
            'parametre'         =>  $this->brassor->getIngredientType()[$ingredient->getTypeIngredient()]['nom']
        ]);
    }

    public function newIngredient($ingredient, $biereId, $ingredientType)
    {
        $biere = $this->brassor->getBiereById($biereId);
                
        if ($biere->getState() > 0) // modification de la recette initiale - Ajout d'un ingrédient en cours de brassage
        {
            $ingredient->setIsBrewed(Brassor::INGREDIENTS_REELS);
        } else  // Création de la recette initiale - Ajout d'un ingrédient avant brassage
        {
            $ingredient->setIsBrewed(Brassor::INGREDIENTS_PREVUS);
        }

        $ingredient->setTypeIngredient($ingredientType);
        $ingredient->setBiereId($biereId);        

        return $ingredient;
    }

    public function isInList($ingredient)
    {
        $ingredientExistant = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
            'stock'    =>   $ingredient->getStock(),
            'biereId'    =>   $ingredient->getBiereId(),
            'is_brewed'  =>  $ingredient->getIsBrewed()
        ]);

        if ($ingredientExistant)
        {
            if ($ingredientExistant->getTempsEbullition() == $ingredient->getTempsEbullition())
            {

            }
            else
            {
                
            }
        }
        else
        {

        }
    }

}

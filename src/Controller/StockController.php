<?php

namespace App\Controller;

use Exception;
use App\Lib\Crud;
use App\Entity\Malts;
use App\Entity\Stock;
use App\Lib\Stockator;
use App\Entity\Levures;
use App\Form\MaltsType;
use App\Form\StockType;
use App\Lib\Utilitaire;
use App\Entity\Additifs;
use App\Entity\Houblons;
use App\Lib\Notificator;
use App\Form\LevuresType;
use App\Entity\Ingredient;
use App\Form\AdditifsType;
use App\Form\HoublonsType;
use App\Form\QuantiteType;
use App\Lib\TypeOfIngredient;
use App\Entity\TypeIngredient;
use Symfony\Component\Form\Form;
use App\Form\InteractionIngredientType;
use App\Lib\StockRegistrator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StockController extends AbstractController
{

    private $entityManager;
    private $session;
    private $stockator;
    private $utilitaire;
    private $notificator;
    private $stockRegistrator;

    // private $notification;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Stockator $stockator, Utilitaire $utilitaire, Notificator $notificator, StockRegistrator $stockRegistrator)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->stockator = $stockator;
        $this->utilitaire = $utilitaire;
        $this->notificator = $notificator;
        $this->stockRegistrator = $stockRegistrator;

    }

    /**
     * @Route("/stock/index", name="indexStock")
     * @return Response
     */
    public function index(): Response
    {
        
        $ingredientTypes = $this->stockator->getIngredientType();
        
        return $this->render("stock/index.html.twig", [
            "ingredientTypes"  =>  $ingredientTypes

        ]);
    }

    /**
     * @Route("/stock/{ingredientType}/view", name="viewStock")
     * @param Request $request
     * @param $ingredientType
     * @return Response
     */
    public function viewStock(Request $request, $ingredientType, $form = null): Response
    {
        return $this->forms($request, $ingredientType);
    }
    
    /**
     * @Route("/stock/{ingredientType}/add", name="addStock")
     * @param Request $request
     * @param $ingredientType
     * @return Response
     */
    public function addStock(Request $request, $ingredientType): Response
    {
        $stock = new Stock;
        $stock->setTypeIngredient($ingredientType);

        return $this->forms($request, $ingredientType, $stock);
    }

    /**
     * @Route("/stock/modify/{id}", name="modifyStock")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function modifyStock(Request $request, $id): Response
    {
        $stock = $this->entityManager->getRepository(Stock::class)->findOneBy([
            'id'    => $id
        ]);
        $oldStock = $stock->copyObject();

        return $this->forms($request, $stock->getTypeIngredient(), $stock, $oldStock);
    }


    public function forms(Request $request, $ingredientType = null, $stock = null, $oldStock = null): Response
    {
        $stocks = $this->entityManager->getRepository(Stock::class)->findBy([
            'typeIngredient'    =>  $ingredientType
        ]);

        /**
         * Cas où on visualise les stocks mais on envoie quand même un formulaire de modif des quantités.
         * Formulaire appelé sur la page par javascript.
         */
        if (!$stock)
        {
            if (!$ingredientType)   {   return $this->notificator->homeReturn("0001");   }
            // dd($ingredientType);

            $form = null;
            $form =  $this->createForm(QuantiteType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $ingredientId = $form->getData()['ingredientId'];
                // dd($form);
                $stock = $this->entityManager->getRepository(Stock::class)->findOneBy([
                    'id'    =>  $ingredientId
                ]);
                $oldStock = $stock->copyObject();
                // dd($ingredientId);
                if ($form->get('toZero')->isClicked()) {
                    $newQuantity = 0;
                } elseif ($form->get('quantityUp')->isClicked()) {
                    $newQuantity = $stock->getQuantite() + $form->getData()['quantity'];
                } elseif ($form->get('quantityDown')->isClicked()) {
                    $newQuantity = $stock->getQuantite() - $form->getData()['quantity'];
                }

                $stock->setQuantite($newQuantity);

                $reason = "Modification de quantité";
                $this->stockRegistrator->addInStockRegister($stock, $reason);

                $this->entityManager->flush();

                return $this->redirectToRoute('viewStock', [
                    'ingredientType' => $ingredientType
                ]);
            }
            $form = $form->createView();
        }
        /**
         * Cas où on envoie un formulaire pour ajouter ou modifier un élément de stock
         */
        else
        {

            if (!$ingredientType)    {   $ingredientType = $stock->getTypeIngredient();   }
            if ($stock->getId()) {
                $buttonLabel = 'Modifier';
            } else {
                $buttonLabel = 'Valider';
            }
            $form = $this->createForm(
                    StockType::class,
                    $stock,
                    [
                        'buttonLabel'       =>  $buttonLabel,
                        'ingredientType'    =>  $stock->getTypeIngredient()
                    ]
                );
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                // if ($form->get('valider')->isClicked()) // si on appuie sur le bouton 'Valider
                // {
                    try {
                        if (!$stock->getId()) {
                            $stock->setTypeIngredient($stock->getTypeIngredient());
                            $user = $this->getUser();
                            $stock->setUser($user);
                            $this->entityManager->persist($stock);
                            $quantite = $form->getData()->getQuantite();
                            if (!$quantite) {
                                $stock->setQuantite(0);
                            }

                            $notification = "L'ingrédient '" . $stock->getNom() . "' a été ajouté";
                            $actionRaison = "Ajout";
                        } else {
                            $notification = "L'ingrédient '" . $stock->getNom() . "' a été modifié";
                            $actionRaison = "Modification";
                        }

                    // foreach ($form->getData() as $field => $value) {
                    //     if ($form->getData()[$field]) {
                    //         $value = str_replace(",", ".", $value);
                    //         $form->getData()[$field] = $value;
                    //     }
                    // }                    
                    // $raison = $actionRaison." du ".$stock->getNom();

                        $this->stockRegistrator->addInStockRegister($stock, $notification);
                        $this->entityManager->flush();
                        

                        $status = 'success';
                    } 
                    catch (Exception $e)
                    {
                        $notification = "L'ingrédient n'a pas été ajoutée/modifié. Au moins un des champs est vide";
                        $status = 'danger';
                        dd($e, $notification);
                        // $this->session->set('message', $message);
                    }
                $this->utilitaire->notify($notification, $status);
                // }
                return $this->redirectToRoute('viewStock', [
                    'ingredientType' => $stock->getTypeIngredient()
                ]);
            }
            $form = $form->createView();           
        } 

        $message = $this->session->remove('message');
        return $this->render('stock/view.html.twig',[
            'form'              =>  $form,
            'formQ'             =>  $form,
            'datas'             =>  $stocks,
            'header'            =>  $this->stockator->getIngredientType()[$ingredientType],
            'ingredientType'    =>  $ingredientType,
            'message'           =>  $message,
            'stock'             =>  $stock,
            'ingredientTypes'   =>  $this->stockator->getIngredientType()
        ]);
    }




    /**
     * @Route("/stock/remove/{id}", name="removeStock")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function removeStock(Request $request, $id): Response
    {

        $stock = $this->entityManager->getRepository(Stock::class)->findOneBy([
            'id'    =>  $id
                ]);
        $ingredients = $this->entityManager->getRepository(Ingredient::class)->findBy([
            'stock' =>  $stock
        ]);
        $ingredientType = $stock->getTypeIngredient();
        if (!$ingredients)
        {
            $this->entityManager->remove($stock);

            $reason = "Suppression d'un stock";

            $this->stockRegistrator->addInStockRegister($stock, $reason);

            $this->entityManager->flush();
            $notification = "l'ingrédient est supprimé.";
            $status = 'success';
        }
        else
        {
            $notification = "Cet ingrédient ne peut-être supprimé. Il est utilisé dans une bière.";
            $status = 'error';
                       
      
        }
        $this->utilitaire->notify($notification, $status);
        
        return $this->redirectToRoute('viewStock', [
            'ingredientType'    =>  $ingredientType
        ]);      
    }     
}

<?php

namespace App\Controller;

use App\Entity\Biere;
use App\Entity\Ingredient;
use App\Entity\Palier;
use App\Entity\TypeIngredient;
use App\Form\EmpatageType;
use App\Form\IngredientType;
use App\Form\PalierType;
use App\Form\RincageType;
use App\Form\VolPrevuType;
use App\Lib\Brassor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BrassageController extends AbstractController
{
    private $entityManager;
    private $session;

    

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Brassor $brassor)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->brassor = $brassor;
    }

    // /**
    //  * @Route("/biere", name="biere")
    //  */
    // public function index(): Response
    // {
    //     return $this->render('biere/index.html.twig', [
    //         'controller_name' => 'BrassageController',
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/debutResume", name="debutResume")
    //  * @param $id
    //  * @param Request $request
    //  * @return Response
    //  */
    
    // public function debutResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);
    //     $typesOfIngredient = $this->entityManager->getRepository(TypeIngredient::class)->findAll();
    //     $ingredients = $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS);
    //     $biere->setState(Biere::DEBUT);
    //     // $biere->setTransition(Brassage::DEBUT);
    //     $this->entityManager->flush();

    //     return $this->render('biere/DebutBrassage.html.twig',[
    //         'biere'             =>  $biere,
    //         'typesOfIngredient' =>  $typesOfIngredient,
    //         'ingredients'       =>  $ingredients
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/VolPrevu", name="volFinalPrevu")
    //  * @param $id
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function volFinalPrevu($biereId, Request $request): Response
    // {
    //     $biere = $this->brassor->getBiereById($biereId);

    //     $volFinalPrevu = $biere->getVolFinalPrevu();
    //     if ($volFinalPrevu) {
    //         $placeholder = $volFinalPrevu . ' Litres';
    //     } else {
    //         $placeholder = 'En litre';
    //     }

    //     $form = $this->createForm(VolPrevuType::class, $biere, [
    //         'placeholder'   =>  $placeholder
    //     ]);
    //     $form->handleRequest($request);
    //     $notification = "Quel volume final du brassin voulez vous?";

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->entityManager->flush();
    //         $notification = 'Le volume de ma bière sera de ' . $biere->getVolFinalPrevu() . ' litres';
    //         return $this->redirectToRoute('indexIngredients', [
    //             'biereId'   =>  $biereId
    //         ]);
    //     }

    //     return $this->render('biere/volFinalPrevu.html.twig', [
    //         'form'  =>  $form->createView(),
    //         'biere'  =>  $biere,
    //         //            'TypesOfIngredient' =>  $this->typesOfIngredient,
    //         'notification'   =>  $notification
    //     ]);       
        
        

    //     $form = $this->createForm(VolPrevuType::class, $biere);
    //     $form->handleRequest($request);

    //     if($form->isSubmitted() && $form->isValid())
    //     {
    //         try
    //         {
    //             $this->entityManager->flush();
    //         }
    //         catch (ORMException $e)
    //         {
    //             // $this->session->add('errors', $e);
    //         }
    //         return $this->redirectToRoute('indexIngredients', [
    //             'biereId'   =>  $biereId
    //         ]);                    
    //     }
    //     return $this->render('biere/volFinalPrevu.html.twig', [
    //         'form'  =>  $form->createView()
    //     ]);        
    // }

    // /**
    //  * @Route("/biere/{biereId}/empatage/resume", name="empatageResume")
    //  * @param $biereId
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function empatageResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);
    //     $paliers = $this->brassor->getPaliersByIdBiere($biereId);
    //     $ingredients = $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS);

    //     // $biere->setTransition(Brassage::EMPATAGE);
    //     $biere->setState(Biere::DEBUT);
    //     $this->entityManager->flush();

        
    //     return $this->render('biere/EmpatageResume.html.twig',[
    //         'biere'  =>  $biere,
    //         'paliers'   =>  $paliers,
    //         'ingredients'   =>  $ingredients
    //     ]);
    // }

    

    // /**
    //  * @Route("/biere/{biereId}/empatage/VH2O", name="empatageVH2O")
    //  * @param $biereId
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function empatageVH2O(Request $request, $biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);

    //     $form = $this->createForm(EmpatageType::class, $biere);
    //     $form->handleRequest($request);
    //     if ($form->isSubmitted() && $form->isValid())
    //     {
    //         $this->entityManager->flush();
    //         return $this->redirectToRoute('empatageResume', [
    //                 'biereId'   =>  $biereId
    //         ]);          
    //     }
    //     return $this->render('biere/EmpatageResume.html.twig',[
    //         'form'  =>  $form,
    //         'biere'  =>  $biere
    //     ]);        
    // }

    // /**
    //  * @Route("/biere/{biereId}/rincage/resume", name="rincageResume")
    //  * @param $biereId
    //  * @return Response
    //  */
    // public function rincageResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);

    //     // $biere->setTransition(Brassage::RINCAGE);
    //     $biere->setState(Biere::EMPATEE);

    //     $this->entityManager->flush(); 

    //     return $this->render('biere/process.html.twig', [
    //         'biere'  =>  $biere,
    //         'ingredients'   =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS),
    //         'typesOfIngredient' =>  $this->entityManager->getRepository(TypeIngredient::class)->findAll(),
    //         'paliers'   =>  $this->brassor->getPaliersByIdBiere($biereId),
    //         // 'etape'     =>  ucfirst($this->constantFwdBrassage[$biere->getState()])
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/rincage/VH2O", name="rincageVH2O")
    //  * @param $id
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function rincageVH2O(Request $request, $biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);

    //     $form = $this->createForm(RincageType::class, $biere);
    //     $form->handleRequest($request);
    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $this->entityManager->flush();
    //         return $this->redirectToRoute('rincageResume', [
    //             'biereId'   =>  $biereId
    //         ]);
    //     }
    //     return $this->render('biere/RincageResume.html.twig', [
    //         'form'  =>  $form,
    //         'biere'  =>  $biere
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/houblonnage/resume", name="houblonnageResume")
    //  * @param $biereId
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function houblonnageResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);
    //     $ingredients = $this->brassor->getIngredientsByIdBiere($biereId);        

    //     // $biere->setTransition(Brassage::HOUBLONNAGE);
    //     $biere->setState(Biere::RINCEE);

    //     $this->entityManager->flush();

    //     return $this->render('biere/HoublonnageResume.html.twig', [
    //         'biere'      =>  $biere,
    //         'ingredients'   =>  $ingredients
    //     ]);
    // }

    /**
     * @Route("/biere/{biereId}/{etape}/add/{typeIngredient}", name="addIngredientBrassage")
     * @param $biereId
     * @param $etape
     * @param $typeIngredient
     * @param Request $request
     * @return Response
     */
    public function addIngredient($biereId, $etape, $typeIngredient, Request $request)
    {
        $ingredient = new Ingredient();
        $ingredient->setTypeIngredient($this->entityManager->getRepository(TypeIngredient::class)->findOneBy(['nom'=>$typeIngredient]));
        return $this->saveIngredient($biereId, $etape, $ingredient, $request);
    }

    /**
     * @Route("/biere/{biereId}/{etape}/modify/ingredient/{ingredientId}", name="modifyIngredientBrassage")
     * @param $biereId
     * @param $etape
     * @param $ingredientId
     * @param Request $request
     * @return Response
     */
    public function modifyIngredient($biereId, $etape, $ingredientId, Request $request)
    {
        $ingredient = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
            'id'    =>  $ingredientId
        ]);
        return $this->saveIngredient($biereId, $etape, $ingredientId, $request);
    }

    
    /**
     * saveIngredient
     *
     * @param  mixed $biereId
     * @param  mixed $etape
     * @param  mixed $ingredient
     * @param  mixed $request
     * @return void
     */
    public function saveIngredient($biereId, $etape, $ingredient, Request $request)
    {
        $biere = $this->brassor->getBiereById($biereId);

        $form = $this->createForm(IngredientType::class, $ingredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$ingredient->getId()) {
                $ingredient->setBiere($biere);
                $this->entityManager->persist($ingredient);
            }
            $this->entityManager->flush();
            return $this->redirectToRoute($etape.'Resume', [
                'biereId'   =>  $biereId
            ]);
        }
        return $this->render('biere/'.$etape.'Resume.html.twig', [
            'form'  =>  $form,
            'biere'      =>  $this->brassor->getBiereById($biereId),
            'ingredients'   =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS)
        ]);
    }

    // /**
    //  * @Route("/biere/{biereId}/{etape}/remove/ingredient/{ingredientId}", name="removeIngredientBrassage")
    //  * @param $biereId
    //  * @param $ingredientId
    //  * @param $etape
    //  * @return Response
    //  */
    // public function removeIngredient($biereId, $ingredientId, $etape)
    // {
    //     $ingredient = $this->entityManager->getRepository(Ingredient::class)->findOneBy([
    //         'id'    =>  $ingredientId
    //     ]);
    //     try {
    //         $this->entityManager->remove($ingredient);
    //         $this->entityManager->flush();
            
    //     } catch (ORMException $e) {
    //         // $this->session->add('errors', $e);
    //     }
    //     return $this->redirectToRoute($etape.'Resume', [
    //         'ingredients'   =>  $this->brassor->getIngredientsByIdBiere($biereId, Brassor::INGREDIENTS_REELS),
    //         'biere'         =>  $this->brassor->getBiereById($biereId)
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/fermentation/resume", name="fermentationResume")
    //  * @param $biereId
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function fermentationResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);
    //     $ingredients = $this->brassor->getIngredientsByIdBiere($biereId);

    //     $biere->setTransition(Biere::FERMENTATION);
    //     $biere->setState(Biere::HOUBLONNEE);
    //     $biere->getBiere()->setState(Biere::BIERE_EN_COURS);

    //     $this->entityManager->flush();

    //     return $this->render('biere/FermentationResume.html.twig', [
    //         'biere'      =>  $biere,
    //         'ingredients'   =>  $ingredients
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/embouteillage/resume", name="embouteillageResume")
    //  * @param $biereId
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function embouteillageResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);
    //     $ingredients = $this->brassor->getIngredientsByIdBiere($biereId);
        
    //     $biere->setTransition(Biere::EMBOUTEILLAGE);
    //     $biere->setState(Biere::FERMENTEE);
    //     $biere->getBiere()->setState(Biere::BIERE_EN_COURS);

    //     $this->entityManager->flush();

    //     return $this->render('biere/EmbouteillageResume.html.twig', [
    //         'biere'      =>  $biere,
    //         'ingredients'   =>  $ingredients
    //     ]);
    // }

    // /**
    //  * @Route("/biere/{biereId}/fiche/resume", name="ficheResume")
    //  * @param $biereId
    //  * @param Request $request
    //  * @return Response
    //  */
    // public function ficheResume($biereId)
    // {
    //     $biere = $this->brassor->getBiereById($biereId);
    //     $ingredients = $this->brassor->getIngredientsByIdBiere($biereId);

    //     // $biere->setTransition(Biere::FINIE);
    //     $biere->setState(Biere::BRASSEE);
    //     // $biere->setState(Biere::EMBOUTEILLEE);

    //     $this->entityManager->flush();

    //     return $this->render('biere/FicheResume.html.twig', [
    //         'biere'      =>  $biere,
    //         'ingredients'   =>  $ingredients
    //     ]);
    // }

     
}
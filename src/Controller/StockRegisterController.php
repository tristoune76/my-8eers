<?php

namespace App\Controller;

use App\Entity\Stock;
use App\Lib\Stockator;
use App\Lib\Utilitaire;
use App\Lib\Notificator;
use App\Lib\StockRegistrator;
use Symfony\Component\Yaml\Yaml;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StockRegisterController extends AbstractController
{

    private $entityManager;
    private $session;
    private $stockator;
    private $utilitaire;
    private $notificator;
    private $stockRegistrator;

    private $parameters;

    // private $notification;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session, Stockator $stockator, Utilitaire $utilitaire, Notificator $notificator, StockRegistrator $stockRegistrator)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
        $this->stockator = $stockator;
        $this->utilitaire = $utilitaire;
        $this->notificator = $notificator;
        $this->stockRegistrator = $stockRegistrator;
        $this->parameters = Yaml::parseFile('../config/parameters/stockRegistrator.yaml');
    }

    /**
     * @Route("/stock/register", name="stock_register")
     */
    public function index(): Response
    {
        return $this->render('stock_register/index.html.twig', [
            'controller_name' => 'StockRegisterController',
        ]);
    }

    /**
     * @Route("/stock/register/{stockId}", name="viewOneStockRegister")
     * @param Request $request
     * @param $stockId
     * @return Response
     */
    public function viewOneStockRegister($stockId): Response
    {
        $stock = $this->entityManager->getRepository(Stock::class)->findOneBy([
            'id'    =>  $stockId
        ]);
        $register = $this->stockRegistrator->readItemsInStockRegister($stock);

        return $this->render('stock/viewRegister.html.twig', [
            'register'  =>  $register,
            'stock'     =>  $stock,
            'headers'   =>  $this->parameters['headers'],
            'ingredientType'    =>  $stock->getTypeIngredient(),
            'ingredientTypes'   =>  $this->stockator->getIngredientType()
        ]);
    }



    /**
     * Get the value of parameters
     */ 
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Set the value of parameters
     *
     * @return  self
     */ 
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }
}

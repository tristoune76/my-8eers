<?php

namespace App\Controller;

use Exception;
use App\Lib\Crud;
use App\Entity\Biere;
use App\Entity\TypeBiere;
use App\Form\TypeBiereType;
use App\Entity\TypeIngredient;
use App\Form\TypeIngredientType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ParametresController extends AbstractController
{

    private $entityManager;
    private $notification = '';
    private $session;
    
    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->entityManager=$entityManager;
        $this->session=$session;
    }

    /**
     * @Route("/parametres/TypeBiere/index", name="indexTypeBiere")
     * @param Request $request
     * @return Response
     */
    public function index($notification = null, $status = null): Response
    {
        $types = $this->entityManager->getRepository(TypeBiere::class)->findAll();
        return $this->render('parametres/index.html.twig', [
            'types'         =>  $types,
            'notification'  =>  $notification,
            'status'        =>  $status
        ]);
    }

    /**
     * @Route("/parametres/TypeBiere/add", name="addTypeBiere")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $type = new TypeBiere();
        return $this->save($request, $type);
    }

    /**
     * @Route("/parametres/TypeBiere/modify/{id}", name="modifyTypeBiere")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function modify(Request $request, $id)
    {
        $type = $this->entityManager->getRepository(TypeBiere::class)->findOneBy([
            'id'    =>  $id
        ]);
        return  $this->save($request, $type);
        
    }

    public function save(Request $request, $type)
    {  
        $form = $this->createForm(TypeBiereType::class, $type);
        $form->handleRequest($request);
        $notification =null;

        if ($form->isSubmitted() && $form->isValid()) {
           
            if ($form->get('valider')->isClicked()) // si on appuie sur le bouton 'Valider
            {
                try 
                {
                    if (!$type->getId()) 
                    {
                        $this->entityManager->persist($type);
                        $notification .= "Le type '" . $type->getNom() . "' a été ajouté";
                    } 
                    else 
                    {
                        $notification .= "Le type '" . $type->getNom() . "' a été modifié";
                    }
                    $this->entityManager->flush();
                    $status = 'danger';
                } 
                catch (NotNullConstraintViolationException $e) {
                    $notification = "Le type n'a pas été ajouté/modifié. Au moins un des champs est vide";
                    $status = 'danger';
                    // $this->session->add('errors', $e);
                }
                return $this->index($notification, $status);
            }
            elseif ($form->get('annuler')->isClicked()) // si on appuie sur le bouton 'Annuler'
            {
                return $this->index();
            }
            // $this->entityManager->flush();
            // return $this->index();
        }
        $types = $this->entityManager->getRepository(TypeBiere::class)->findAll();
        return $this->render('parametres/save.html.twig', [
            'form'  =>  $form->createView(),
            'types' => $types,
            'type'  => $type
        ]);
    }

    /**
     * @Route("/parametres/TypeBiere/remove/{id}", name="removeTypeBiere")
     * @param $id
     * @return Response
     */
    public function removeTypeBiere($id): Response
    {
        $type = $this->entityManager->getRepository(TypeBiere::class)->findOneBy([
            'id'  =>  $id
            ]);
        // $bieres = $this->entityManager->getRepository(Biere::class)->findByType($type);
        try
        {
            $this->entityManager->remove($type);
            $this->entityManager->flush();
            $notification = "suppression du type '" . $type . "' réussie.";
            $status = 'success';
        }
        catch (ForeignKeyConstraintViolationException $e)
        {
            $notification = "Certaines de vos bières sont du type '" . $type . "'. Vous ne pouvez pas le supprimer.";
            $status = 'danger';
            // $this->session->add('errors', $e);
        }
        return $this->index($notification, $status);

        
        return $this->redirect('/parametres/TypeBiere/index');
    }
}
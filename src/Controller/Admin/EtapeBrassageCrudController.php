<?php

namespace App\Controller\Admin;

use App\Entity\EtapeBrassage;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EtapeBrassageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EtapeBrassage::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}

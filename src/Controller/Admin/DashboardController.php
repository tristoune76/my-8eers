<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\EtapeBrassage;
use App\Entity\TypeIngredient;
use App\Entity\ParametreBrassage;
use App\Entity\Stock;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

// /**
//  * @IsGranted("ROLE_ADMIN")
//  */
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('My 8eers');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateur', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud("Type d'ingrédient", 'fas fa-user', TypeIngredient::class);
        yield MenuItem::linkToCrud("Stock", 'fas fa-user', Stock::class);

    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\Stock;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class StockCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stock::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            NumberField::new('quantite'),
            TextField::new('aromes'),
            NumberField::new('EBCMin'),
            NumberField::new('EBCMax'),
            NumberField::new('acidesAlphaMin'),
            NumberField::new('acidesAlphaMax'),
            TextField::new('type'),
            TextareaField::new('description'),
            TextField::new('typeIngredient'),            
        ];
    }
}

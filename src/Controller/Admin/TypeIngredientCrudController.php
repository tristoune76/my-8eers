<?php

namespace App\Controller\Admin;

use App\Entity\TypeIngredient;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TypeIngredientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypeIngredient::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextField::new('unite'),
            TextField::new('header'),
            TextEditorField::new('description'),
            ArrayField::new('ingredientViewField')



        ];
    }
}

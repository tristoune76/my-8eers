<?php

namespace App\Controller\Admin;

use App\Entity\ParametreBrassage;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ParametreBrassageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ParametreBrassage::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}

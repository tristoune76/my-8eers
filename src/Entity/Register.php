<?php

namespace App\Entity;

use DateTimeZone;
use App\Entity\Stock;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\StockRegisterRepository;

Abstract class Register
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateLog;

    /**
     * @ORM\Column(type="integer")
     */
    private $userId;

    public function hydrate($donnees)
    {
        foreach ($donnees as $attribut => $valeur) {
            $methode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));

            if (is_callable(array($this, $methode))) {
                $this->$methode($valeur);
            }
        }
    }

     public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of dateLog
     */ 
    public function getDateLog()
    {
        return $this->dateLog;
    }

    /**
     * Set the value of dateLog
     *
     * @return  self
     */ 
    public function setDateLog($dateLog)
    {
        $this->dateLog = $dateLog;

        return $this;
    }

    /**
     * Get the value of vars
     */
    public function getVars()
    {
        return get_class_vars(get_class($this));
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }





}






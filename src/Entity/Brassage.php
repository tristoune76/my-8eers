<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Brassage
 *
 * @ORM\Table(name="brassage", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_D63A399FA71147CC", columns={"biere_id"})})
 * @ORM\Entity
 */
class Brassage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vol_final_prevu", type="float", precision=10, scale=0, nullable=true)
     */
    private $volFinalPrevu;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tps_empatage", type="float", precision=10, scale=0, nullable=true)
     */
    private $tpsEmpatage;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vol_h2_oempatage", type="float", precision=10, scale=0, nullable=true)
     */
    private $volH2Oempatage;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vol_h2_orincage", type="float", precision=10, scale=0, nullable=true)
     */
    private $volH2Orincage;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vol_houblonnage", type="float", precision=10, scale=0, nullable=true)
     */
    private $volHoublonnage;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tps_houblonnage", type="integer", nullable=true)
     */
    private $tpsHoublonnage;

    /**
     * @var int|null
     *
     * @ORM\Column(name="d_initiale", type="integer", nullable=true)
     */
    private $dInitiale;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vol_fermentation", type="float", precision=10, scale=0, nullable=true)
     */
    private $volFermentation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tps_fermentation", type="integer", nullable=true)
     */
    private $tpsFermentation;

    /**
     * @var int|null
     *
     * @ORM\Column(name="d_finale", type="integer", nullable=true)
     */
    private $dFinale;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sucre", type="integer", nullable=true)
     */
    private $sucre;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vol_final_obtenu", type="float", precision=10, scale=0, nullable=true)
     */
    private $volFinalObtenu;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=false)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="transition", type="string", length=255, nullable=false)
     */
    private $transition;

    /**
     * @var \Biere
     *
     * @ORM\ManyToOne(targetEntity="Biere")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="biere_id", referencedColumnName="id")
     * })
     */
    private $biere;


}

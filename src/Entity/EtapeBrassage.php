<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EtapeBrassage
 *
 * @ORM\Table(name="etape_brassage")
 * @ORM\Entity
 */
class EtapeBrassage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer", nullable=false)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;


}

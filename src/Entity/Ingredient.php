<?php

namespace App\Entity;

use App\Repository\IngredientsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IngredientsRepository::class)
 */
class Ingredient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $quantite;

    // /**
    //  * @ORM\ManyToOne(targetEntity=TypeIngredient::class, inversedBy="ingredients")
    //  * @ORM\JoinColumn(nullable=false)
    //  */
    // private $typeIngredient;

    /**
     * @ORM\Column(type="string", length=255)
     */

    private $typeIngredient;


    // /**
    //  * @ORM\Column(type="string", length=255)
    //  */
    // private $stock;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $biereId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tempsEbullition;

    /**
     * @ORM\ManyToOne(targetEntity=Stock::class, inversedBy="ingredients")
     */
    private $stock;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_brewed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    public function setQuantite(float $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getTempsEbullition(): ?int
    {
        return $this->tempsEbullition;
    }

    public function setTempsEbullition(?int $tempsEbullition): self
    {
        $this->tempsEbullition = $tempsEbullition;

        return $this;
    }

    // /**
    //  * Get the value of TypeIngredient
    //  */ 
    // public function getTypeIngredient()
    // {
    //     return $this->TypeIngredient;
    // }

    // /**
    //  * Set the value of TypeIngredient
    //  *
    //  * @return  self
    //  */ 
    // public function setTypeIngredient($TypeIngredient)
    // {
    //     $this->TypeIngredient = $TypeIngredient;

    //     return $this;
    // }

    // /**
    //  * Get the value of stock
    //  */ 
    // public function getStock()
    // {
    //     return $this->stock;
    // }

    // /**
    //  * Set the value of stock
    //  *
    //  * @return  self
    //  */ 
    // public function setStock($stock)
    // {
    //     $this->stock = $stock;

    //     return $this;
    // }

    /**
     * Get the value of biereId
     */ 
    public function getBiereId()
    {
        return $this->biereId;
    }

    /**
     * Set the value of biereId
     *
     * @return  self
     */ 
    public function setBiereId($biereId)
    {
        $this->biereId = $biereId;

        return $this;
    }

    // public function getTypeIngredient(): ?TypeIngredient
    // {
    //     return $this->typeIngredient;
    // }

    // public function setTypeIngredient(?TypeIngredient $typeIngredient): self
    // {
    //     $this->typeIngredient = $typeIngredient;

    //     return $this;
    // }

    /**
     * Get the value of typeIngredient
     */ 
    public function getTypeIngredient()
    {
        return $this->typeIngredient;
    }

    /**
     * Set the value of typeIngredient
     *
     * @return  self
     */ 
    public function setTypeIngredient($typeIngredient)
    {
        $this->typeIngredient = $typeIngredient;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getIsBrewed(): ?bool
    {
        return $this->is_brewed;
    }

    public function setIsBrewed(?bool $is_brewed): self
    {
        $this->is_brewed = $is_brewed;

        return $this;
    }

    public function copyObject()
    {
        $class = get_class($this);
        
        $vars = get_class_vars($class);
        $newObject = new $class;
        foreach ($vars as $var => $data) {
            $setMethode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));
            $getMethode = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));

            if (is_callable(array($this, $getMethode)) && is_callable(array($this, $setMethode))) {
                $newObject->$setMethode($this->$getMethode());
            }
        }
        // dd($newObject);
        return $newObject;
    }
}

<?php

namespace App\Entity;

use App\Repository\PalierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PalierRepository::class)
 */
class Palier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Biere::class, inversedBy="paliers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $biere;

    /**
     * @ORM\Column(type="integer")
     */
    private $temps;

    /**
     * @ORM\Column(type="float")
     */
    private $temperature;

    public function getId(): ?int
    {
        return $this->id;
    }

    // public function getBrassage(): ?Brassage
    // {
    //     return $this->brassage;
    // }

    // public function setBrassage(?Brassage $brassage): self
    // {
    //     $this->brassage = $brassage;

    //     return $this;
    // }

    public function getTemps(): ?int
    {
        return $this->temps;
    }

    public function setTemps(int $temps): self
    {
        $this->temps = $temps;

        return $this;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get the value of biere
     */ 
    public function getBiere()
    {
        return $this->biere;
    }

    /**
     * Set the value of biere
     *
     * @return  self
     */ 
    public function setBiere($biere)
    {
        $this->biere = $biere;

        return $this;
    }
}

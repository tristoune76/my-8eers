<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    // /**
    //  * @ORM\ManyToOne(targetEntity=TypeIngredient::class, inversedBy="stocks")
    //  * @ORM\JoinColumn(nullable=false)
    //  */
    // private $typeIngredient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeIngredient;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $aromes;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $EBCMin;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $EBCMax;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $acidesAlphaMin;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $acidesAlphaMax;

    // /**
    //  * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="stock")
    //  */
    // private $ingredients;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="stock")
     */
    private $ingredients;

    public function __construct()
    {
        // $this->ingredients = new ArrayCollection();
    }

     public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    // public function getTypeIngredient(): ?TypeIngredient
    // {
    //     return $this->typeIngredient;
    // }

    // public function setTypeIngredient(?TypeIngredient $typeIngredient): self
    // {
    //     $this->typeIngredient = $typeIngredient;

    //     return $this;
    // }

    public function getQuantite(): ?float
    {
        return $this->quantite;
    }

    public function setQuantite(?float $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getAromes(): ?string
    {
        return $this->aromes;
    }

    public function setAromes(?string $aromes): self
    {
        $this->aromes = $aromes;

        return $this;
    }

    public function getEBCMin(): ?float
    {
        return $this->EBCMin;
    }

    public function setEBCMin(?float $EBCMin): self
    {
        $this->EBCMin = $EBCMin;

        return $this;
    }

    public function getEBCMax(): ?float
    {
        return $this->EBCMax;
    }

    public function setEBCMax(?float $EBCMax): self
    {
        $this->EBCMax = $EBCMax;

        return $this;
    }

    public function getAcidesAlphaMin(): ?float
    {
        return $this->acidesAlphaMin;
    }

    public function setAcidesAlphaMin(?float $acidesAlphaMin): self
    {
        $this->acidesAlphaMin = $acidesAlphaMin;

        return $this;
    }

    public function getAcidesAlphaMax(): ?float
    {
        return $this->acidesAlphaMax;
    }

    public function setAcidesAlphaMax(?float $acidesAlphaMax): self
    {
        $this->acidesAlphaMax = $acidesAlphaMax;

        return $this;
    }

    // /**
    //  * @return Collection|Ingredient[]
    //  */
    // public function getIngredients(): Collection
    // {
    //     return $this->ingredients;
    // }

    // public function addIngredient(Ingredient $ingredient): self
    // {
    //     if (!$this->ingredients->contains($ingredient)) {
    //         $this->ingredients[] = $ingredient;
    //         $ingredient->setStock($this);
    //     }

    //     return $this;
    // }

    // public function removeIngredient(Ingredient $ingredient): self
    // {
    //     if ($this->ingredients->removeElement($ingredient)) {
    //         // set the owning side to null (unless already changed)
    //         if ($ingredient->getStock() === $this) {
    //             $ingredient->setStock(null);
    //         }
    //     }

    //     return $this;
    // }

    public function __toString(): string
    {
        $return = $this->getNom();
        $unite = 'g';
        if($this->typeIngredient == 'houblon')
        {
            $return .=  ' - '.$this->getType().' - (AA Min: '.$this->getAcidesAlphaMin().', Max: '.$this->getAcidesAlphaMax().') - '.$this->getAromes();
        }
        elseif ($this->typeIngredient == 'grain')
        {
            $return .= ' - (EBC Min: '.$this->getEBCMin().', Max: '.$this->getEBCMax().')';
            $unite = 'Kg';
        }
        $return .= ' - Reste: '.$this->getQuantite().' '.$unite;

        return $return;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }




    /**
     * Get the value of typeIngredient
     */ 
    public function getTypeIngredient()
    {
        return $this->typeIngredient;
    }

    /**
     * Set the value of typeIngredient
     *
     * @return  self
     */ 
    public function setTypeIngredient($typeIngredient)
    {
        $this->typeIngredient = $typeIngredient;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
            $ingredient->setStock($this);
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        if ($this->ingredients->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getStock() === $this) {
                $ingredient->setStock(null);
            }
        }

        return $this;
    }

    public function copyObject()
    {
        $class = get_class($this);

        $vars = get_class_vars($class);
        $newObject = new $class;
        foreach ($vars as $var => $data) {
            $setMethode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));
            $getMethode = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $var)));

            if (is_callable(array($this, $getMethode)) && is_callable(array($this, $setMethode))) {
                $newObject->$setMethode($this->$getMethode());
            }
        }
        // dd($newObject);
        return $newObject;
    }

    /**
     * Get the value of vars
     */
    public function getVars()
    {
        return get_class_vars(get_class($this));
    }
}

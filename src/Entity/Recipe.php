<?php

namespace App\Entity;

use App\Repository\RecipeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RecipeRepository::class)
 */
class Recipe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $biereId;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $houblons = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $grains = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $levures = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $additifs = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="recipes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $palier = [];

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volH2OEmpatage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volH2ORincage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volHoublonnage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volEmpatage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volFinalObtenu;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volFermentation;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $dInitiale;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fFinale;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $dRincage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBiereId(): ?int
    {
        return $this->biereId;
    }

    public function setBiereId(int $biereId): self
    {
        $this->biereId = $biereId;

        return $this;
    }

    public function getHoublons(): ?array
    {
        return $this->houblons;
    }

    public function setHoublons(array $houblons): self
    {
        $this->houblons = $houblons;

        return $this;
    }

    public function getGrains(): ?array
    {
        return $this->grains;
    }

    public function setGrains(array $grains): self
    {
        $this->grains = $grains;

        return $this;
    }

    public function getLevures(): ?array
    {
        return $this->levures;
    }

    public function setLevures(array $levures): self
    {
        $this->levures = $levures;

        return $this;
    }

    public function getAdditifs(): ?array
    {
        return $this->additifs;
    }

    public function setAdditifs(array $additifs): self
    {
        $this->additifs = $additifs;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getPalier(): ?array
    {
        return $this->palier;
    }

    public function setPalier(?array $palier): self
    {
        $this->palier = $palier;

        return $this;
    }

    public function getVolH2OEmpatage(): ?float
    {
        return $this->volH2OEmpatage;
    }

    public function setVolH2OEmpatage(?float $volH2OEmpatage): self
    {
        $this->volH2OEmpatage = $volH2OEmpatage;

        return $this;
    }

    public function getVolH2ORincage(): ?float
    {
        return $this->volH2ORincage;
    }

    public function setVolH2ORincage(float $volH2ORincage): self
    {
        $this->volH2ORincage = $volH2ORincage;

        return $this;
    }

    public function getVolHoublonnage(): ?float
    {
        return $this->volHoublonnage;
    }

    public function setVolHoublonnage(?float $volHoublonnage): self
    {
        $this->volHoublonnage = $volHoublonnage;

        return $this;
    }

    public function getVolEmpatage(): ?float
    {
        return $this->volEmpatage;
    }

    public function setVolEmpatage(?float $volEmpatage): self
    {
        $this->volEmpatage = $volEmpatage;

        return $this;
    }

    public function getVolFinalObtenu(): ?float
    {
        return $this->volFinalObtenu;
    }

    public function setVolFinalObtenu(?float $volFinalObtenu): self
    {
        $this->volFinalObtenu = $volFinalObtenu;

        return $this;
    }

    public function getVolFermentation(): ?float
    {
        return $this->volFermentation;
    }

    public function setVolFermentation(?float $volFermentation): self
    {
        $this->volFermentation = $volFermentation;

        return $this;
    }

    public function getDInitiale(): ?float
    {
        return $this->dInitiale;
    }

    public function setDInitiale(?float $dInitiale): self
    {
        $this->dInitiale = $dInitiale;

        return $this;
    }

    public function getFFinale(): ?float
    {
        return $this->fFinale;
    }

    public function setFFinale(?float $fFinale): self
    {
        $this->fFinale = $fFinale;

        return $this;
    }

    public function getDRincage(): ?float
    {
        return $this->dRincage;
    }

    public function setDRincage(?float $dRincage): self
    {
        $this->dRincage = $dRincage;

        return $this;
    }
}

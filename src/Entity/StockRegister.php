<?php

namespace App\Entity;

use DateTimeZone;
use App\Entity\Stock;
use App\Entity\Register;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\StockRegisterRepository;

/**
 * @ORM\Entity(repositoryClass=StockRegisterRepository::class)
 */
class StockRegister extends Register
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $stockId;
    
    // /**
    //  * @ORM\Column(type="datetime")
    //  */
    // private $dateLog;

    /**
     * @ORM\Column(type="float")
     */
    private $quantite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reason;

    // /**
    //  * @ORM\Column(type="integer")
    //  */
    // private $userId;

    public function __construct($valeurs = array())
    {
        if (!empty($valeurs))
            $this->hydrate($valeurs);

        $this->setDateLog(new \DateTime());

    }

    // public function hydrate($donnees)
    // {
    //     foreach ($donnees as $attribut => $valeur) {
    //         $methode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));

    //         if (is_callable(array($this, $methode))) {
    //             $this->$methode($valeur);
    //         }
    //     }
        
    // }
    
    public function getId(): ?int
    {
        return $this->id;
    }  

    // /**
    //  * Get the value of dateLog
    //  */ 
    // public function getDateLog()
    // {
    //     return $this->dateLog;
    // }

    // /**
    //  * Set the value of dateLog
    //  *
    //  * @return  self
    //  */ 
    // public function setDateLog($dateLog)
    // {
    //     $this->dateLog = $dateLog;

    //     return $this;
    // }

    // /**
    //  * Get the value of vars
    //  */
    // public function getVars()
    // {
    //     return get_class_vars(get_class($this));
    // }
    

       /**
     * Get the value of stockId
     */ 
    public function getStockId()
    {
        return $this->stockId;
    }

    /**
     * Set the value of stockId
     *
     * @return  self
     */ 
    public function setStockId($stockId)
    {
        $this->stockId = $stockId;

        return $this;
    }

    /**
     * Get the value of quantite
     */ 
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set the value of quantite
     *
     * @return  self
     */ 
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    // public function getUserId(): ?int
    // {
    //     return $this->userId;
    // }

    // public function setUserId(int $userId): self
    // {
    //     $this->userId = $userId;

    //     return $this;
    // }

    /**
     * Get the value of reason
     */ 
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set the value of reason
     *
     * @return  self
     */ 
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }
}

<?php

namespace App\Entity;

use DateTimeZone;
use App\Entity\Biere;
use App\Entity\Register;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BiereRegisterRepository;

/**
 * @ORM\Entity(repositoryClass=BiereRegisterRepository::class)
 */
class BiereRegister extends Biere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $biereId;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $dateLog;    


    public function __construct($valeurs = array())
    {
        if (!empty($valeurs))
            $this->hydrate($valeurs);

        $this->setDateLog(new \DateTime());

    }

    public function hydrate($donnees)
    {
        foreach ($donnees as $attribut => $valeur) {
            $methode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));

            if (is_callable(array($this, $methode))) {
                $this->$methode($valeur);
            }
        }
        
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBiereId(): ?int
    {
        return $this->biereId;
    }

    public function setBiereId(int $BiereId): self
    {
        $this->biereId = $biereId;

        return $this;
    }     

    /**
     * Get the value of dateLog
     */ 
    public function getDateLog()
    {
        return $this->dateLog;
    }

    /**
     * Set the value of dateLog
     *
     * @return  self
     */ 
    public function setDateLog($dateLog)
    {
        $this->dateLog = $dateLog;

        return $this;
    }

    /**
     * Get the value of vars
     */
    public function getVars()
    {
        return get_class_vars(get_class($this));
    }
    
}

<?php

namespace App\Entity;

use ArrayAccess;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BiereRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=BiereRepository::class)
 */
class Biere implements ArrayAccess
{

    const   PROJET      =   0;
    const   PRETE       =   1;
    const   LANCEE      =   2;
    const   EMPATEE     =   3;
    const   RINCEE      =   4;
    const   HOUBLONNEE  =   5;
    const   FERMENTEE   =   6;
    const   BRASSEE     =   7;
    const   STOP        =   10;
    // const   EMBOUTEILLEE   =   7;
    // const   BRASSEE     = 8;

    private $container = array();

   
     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAjout;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateBrassage;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateBrassee;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=TypeBiere::class, inversedBy="bieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

//    /**
//     * @ORM\OneToMany(targetEntity=IngRecette::class, mappedBy="idBiere")
//     */
//    private $ingRecettes;

    // /**
    //  * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="idBiere")
    //  */
    // private $ingredientsPourRecettes;

    // /**
    //  * @ORM\OneToMany(targetEntity=Ingredient::class, mappedBy="biere")
    //  */
    // private $ingredients;

    /**
     * @ORM\Column(type="string")
     */
    private $state;

    // /**
    //  * @ORM\Column(type="boolean")
    //  */
    // private $ingredientState;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    // /**
    //  * @ORM\OneToOne(targetEntity=Brassage::class, mappedBy="biere", cascade={"persist", "remove"})
    //  */
    // private $brassage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currentPlace;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volFinalPrevu;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tpsEmpatage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volH2OEmpatage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volH2ORincage;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volHoublonnage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tpsHoublonnage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dInitiale;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volFermentation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tpsFermentation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dFinale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sucre;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volFinalObtenu;

    /**
     * @ORM\OneToMany(targetEntity=Palier::class, mappedBy="biere")
     */
    private $paliers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transition;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $poidsGrain;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volEmpatage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dRincage;

    public function __construct($valeurs = array())
    // public function __construct()
    {
        if (!empty($valeurs)) {
            $this->hydrate($valeurs);
        }
        // $this->ingredients = new ArrayCollection();
        $this->paliers = new ArrayCollection();

    }

    // public function __construct()
    // {
    //     $this->container = array(
    //         "one"   => 1,
    //         "two"   => 2,
    //         "three" => 3,
    //     );
    // }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }


    // public function offsetExists($offset) { }

    // public function offsetGet($offset) { }

    // public function offsetSet($offset, $value) { }

    // public function offsetUnset($offset) { }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateAjout(): ?\DateTimeInterface
    {
        return $this->dateAjout;
    }

    public function setDateAjout(\DateTimeInterface $dateAjout): self
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?typeBiere
    {
        return $this->type;
    }

    public function setType(?typeBiere $type): self
    {
        $this->type = $type;

        return $this;
    }

    // /**
    //  * @return Collection|Ingredient[]
    //  */
    // public function getIngredientsPourRecettes(): Collection
    // {
    //     return $this->ingredientsPourRecettes;
    // }

    // public function addIngredientsPourRecette(Ingredient $ingredientsPourRecette): self
    // {
    //     if (!$this->ingredientsPourRecettes->contains($ingredientsPourRecette)) {
    //         $this->ingredientsPourRecettes[] = $ingredientsPourRecette;
    //         $ingredientsPourRecette->setIdBiere($this);
    //     }

    //     return $this;
    // }

    // public function removeIngredientsPourRecette(Ingredient $ingredientsPourRecette): self
    // {
    //     if ($this->ingredientsPourRecettes->removeElement($ingredientsPourRecette)) {
    //         // set the owning side to null (unless already changed)
    //         if ($ingredientsPourRecette->getIdBiere() === $this) {
    //             $ingredientsPourRecette->setIdBiere(null);
    //         }
    //     }

    //     return $this;
    // }

    
    // /**
    //  * @return Collection|Ingredient[]
    //  */
    // public function getIngredients(): Collection
    // {
    //     return $this->ingredients;
    // }

    // public function addIngredient(Ingredient $ingredient): self
    // {
    //     if (!$this->ingredients->contains($ingredient)) {
    //         $this->ingredients[] = $ingredient;
    //         $ingredient->setBiereId($this->id);
    //     }

    //     return $this;
    // }

    // public function removeIngredient(Ingredient $ingredient): self
    // {
    //     if ($this->ingredients->removeElement($ingredient)) {
    //         // set the owning side to null (unless already changed)
    //         if ($ingredient->getBiereId() === $this->id) {
    //             $ingredient->setBiereId(null);
    //         }
    //     }

    //     return $this;
    // }

    public function getState()
    {
        return $this->state;
    }

    public function setState($state): self
    {
        $this->state = $state;

        return $this;
    }

    // public function getIngredientState(): ?bool
    // {
    //     return $this->ingredientState;
    // }

    // public function setIngredientState(bool $ingredientState): self
    // {
    //     $this->ingredientState = $ingredientState;

    //     return $this;
    // }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    // public function getBrassage(): ?Brassage
    // {
    //     return $this->brassage;
    // }

    // public function setBrassage(Brassage $brassage): self
    // {
    //     // set the owning side of the relation if necessary
    //     if ($brassage->getBiere() !== $this) {
    //         $brassage->setBiere($this);
    //     }

    //     $this->brassage = $brassage;

    //     return $this;
    // }

    public function getCurrentPlace(): ?string
    {
        return $this->currentPlace;
    }

    public function setCurrentPlace(?string $currentPlace): self
    {
        $this->currentPlace = $currentPlace;

        return $this;
    }
    

    /**
     * Get the value of dateBrassage
     */ 
    public function getDateBrassage()
    {
        return $this->dateBrassage;
    }

    /**
     * Set the value of dateBrassage
     *
     * @return  self
     */ 
    public function setDateBrassage($dateBrassage)
    {
        $this->dateBrassage = $dateBrassage;

        return $this;
    }

    /**
     * Get the value of dateBrassee
     */ 
    public function getDateBrassee()
    {
        return $this->dateBrassee;
    }

    /**
     * Set the value of dateBrassee
     *
     * @return  self
     */ 
    public function setDateBrassee($dateBrassee)
    {
        $this->dateBrassee = $dateBrassee;

        return $this;
    }

    /**
     * Get the value of volFinalPrevu
     */ 
    public function getVolFinalPrevu()
    {
        return $this->volFinalPrevu;
    }

    /**
     * Set the value of volFinalPrevu
     *
     * @return  self
     */ 
    public function setVolFinalPrevu($volFinalPrevu)
    {
        $this->volFinalPrevu = $volFinalPrevu;

        return $this;
    }

    /**
     * Get the value of tpsEmpatage
     */ 
    public function getTpsEmpatage()
    {
        return $this->tpsEmpatage;
    }

    /**
     * Set the value of tpsEmpatage
     *
     * @return  self
     */ 
    public function setTpsEmpatage($tpsEmpatage)
    {
        $this->tpsEmpatage = $tpsEmpatage;

        return $this;
    }

    /**
     * Get the value of volH2OEmpatage
     */ 
    public function getVolH2OEmpatage()
    {
        return $this->volH2OEmpatage;
    }

    /**
     * Set the value of volH2OEmpatage
     *
     * @return  self
     */ 
    public function setVolH2OEmpatage($volH2OEmpatage)
    {
        $this->volH2OEmpatage = $volH2OEmpatage;

        return $this;
    }

    /**
     * Get the value of volH2ORincage
     */ 
    public function getVolH2ORincage()
    {
        return $this->volH2ORincage;
    }

    /**
     * Set the value of volH2ORincage
     *
     * @return  self
     */ 
    public function setVolH2ORincage($volH2ORincage)
    {
        $this->volH2ORincage = $volH2ORincage;

        return $this;
    }

    /**
     * Get the value of volHoublonnage
     */ 
    public function getVolHoublonnage()
    {
        return $this->volHoublonnage;
    }

    /**
     * Set the value of volHoublonnage
     *
     * @return  self
     */ 
    public function setVolHoublonnage($volHoublonnage)
    {
        $this->volHoublonnage = $volHoublonnage;

        return $this;
    }

    /**
     * Get the value of tpsHoublonnage
     */ 
    public function getTpsHoublonnage()
    {
        return $this->tpsHoublonnage;
    }

    /**
     * Set the value of tpsHoublonnage
     *
     * @return  self
     */ 
    public function setTpsHoublonnage($tpsHoublonnage)
    {
        $this->tpsHoublonnage = $tpsHoublonnage;

        return $this;
    }

    /**
     * Get the value of dInitiale
     */ 
    public function getDInitiale()
    {
        return $this->dInitiale;
    }

    /**
     * Set the value of dInitiale
     *
     * @return  self
     */ 
    public function setDInitiale($dInitiale)
    {
        $this->dInitiale = $dInitiale;

        return $this;
    }

    /**
     * Get the value of volFermentation
     */ 
    public function getVolFermentation()
    {
        return $this->volFermentation;
    }

    /**
     * Set the value of volFermentation
     *
     * @return  self
     */ 
    public function setVolFermentation($volFermentation)
    {
        $this->volFermentation = $volFermentation;

        return $this;
    }

    /**
     * Get the value of tpsFermentation
     */ 
    public function getTpsFermentation()
    {
        return $this->tpsFermentation;
    }

    /**
     * Set the value of tpsFermentation
     *
     * @return  self
     */ 
    public function setTpsFermentation($tpsFermentation)
    {
        $this->tpsFermentation = $tpsFermentation;

        return $this;
    }

    /**
     * Get the value of dFinale
     */ 
    public function getDFinale()
    {
        return $this->dFinale;
    }

    /**
     * Set the value of dFinale
     *
     * @return  self
     */ 
    public function setDFinale($dFinale)
    {
        $this->dFinale = $dFinale;

        return $this;
    }

    /**
     * Get the value of sucre
     */ 
    public function getSucre()
    {
        return $this->sucre;
    }

    /**
     * Set the value of sucre
     *
     * @return  self
     */ 
    public function setSucre($sucre)
    {
        $this->sucre = $sucre;

        return $this;
    }

    /**
     * Get the value of volFinalObtenu
     */ 
    public function getVolFinalObtenu()
    {
        return $this->volFinalObtenu;
    }

    /**
     * Set the value of volFinalObtenu
     *
     * @return  self
     */ 
    public function setVolFinalObtenu($volFinalObtenu)
    {
        $this->volFinalObtenu = $volFinalObtenu;

        return $this;
    }

    /**
     * Get the value of paliers
     */ 
    public function getPaliers()
    {
        return $this->paliers;
    }

    /**
     * Set the value of paliers
     *
     * @return  self
     */ 
    public function setPaliers($paliers)
    {
        $this->paliers = $paliers;

        return $this;
    }

    /**
     * Get the value of transition
     */ 
    public function getTransition()
    {
        return $this->transition;
    }

    /**
     * Set the value of transition
     *
     * @return  self
     */ 
    public function setTransition($transition)
    {
        $this->transition = $transition;

        return $this;
    }

    public function hydrate($donnees)
    {
        foreach ($donnees as $attribut => $valeur) {
            $methode = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));

            if (is_callable(array($this, $methode))) {
                $this->$methode($valeur);
            }
        }
    }

    public function getPoidsGrain(): ?float
    {
        return $this->poidsGrain;
    }

    public function setPoidsGrain(?float $poidsGrain): self
    {
        $this->poidsGrain = $poidsGrain;

        return $this;
    }

    public function getVolEmpatage(): ?float
    {
        return $this->volEmpatage;
    }

    public function setVolEmpatage(?float $volEmpatage): self
    {
        $this->volEmpatage = $volEmpatage;

        return $this;
    }

    public function addPoidsGrain($poidsSup)
    {
        $this->setPoidsGrain($this->getPoidsGrain() + $poidsSup);
    }

    public function getDRincage(): ?int
    {
        return $this->dRincage;
    }

    public function setDRincage(?int $dRincage): self
    {
        $this->dRincage = $dRincage;

        return $this;
    }
}

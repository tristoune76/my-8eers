<?php

namespace App\Repository;

use App\Entity\Biere;
use App\Entity\TypeBiere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Biere|null find($id, $lockMode = null, $lockVersion = null)
 * @method Biere|null findOneBy(array $criteria, array $orderBy = null)
 * @method Biere[]    findAll()
 * @method Biere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BiereRepository extends EntityRepository
{
    private $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Biere::class);
        $this->entityManager = $entityManager;
    }

    /**
    * @return Biere[] Returns an array of Biere objects
    */

    public function findByType($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.type = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Biere
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    // /**
    //  * @return array
    //  */

    // public function getFull(): array
    // {
    //     $bieres = $this->entityManager->getRepository(Biere::class)->findAll();
    //     $bieresFull=[];
    //     foreach ($bieres as $biere)
    //     {
    //         $typeBiere = $this->entityManager->getRepository(TypeBiere::class)->findOneById($biere->getType());
    //         if (!$typeBiere)
    //         {
    //             $typeBiere = 'type non spécifié';
    //         }
    //         else
    //         {
    //             $typeBiere = $typeBiere->getNom();
    //         }
    //         $bieresFull [] = [
    //             'id'            =>  $biere->getId(),
    //             'nom'           =>  $biere->getNom(),
    //             'type'          =>  $typeBiere,
    //             'description'   =>  $biere->getDescription(),
    //             'dateAjout'     =>  $biere->getDateAjout()->format('d-m-Y'),
    //             'ingredientState' =>    $biere->getIngredientState()
    //         ];
    //     }
    //     return $bieresFull;

    // }

    
    public function getByType()
    {}
}

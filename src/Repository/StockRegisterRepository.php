<?php

namespace App\Repository;

use App\Entity\StockRegister;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockRegister|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockRegister|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockRegister[]    findAll()
 * @method StockRegister[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockRegisterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockRegister::class);
    }

    // /**
    //  * @return StockRegister[] Returns an array of StockRegister objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StockRegister
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

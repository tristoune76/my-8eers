<?php

namespace App\Repository;

use App\Entity\ParametreBrassage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParametreBrassage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParametreBrassage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParametreBrassage[]    findAll()
 * @method ParametreBrassage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParametreBrassageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParametreBrassage::class);
    }

    // /**
    //  * @return ParametreBrassage[] Returns an array of ParametreBrassage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParametreBrassage
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

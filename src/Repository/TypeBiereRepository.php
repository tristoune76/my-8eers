<?php

namespace App\Repository;

use App\Entity\TypeBiere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeBiere|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeBiere|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeBiere[]    findAll()
 * @method TypeBiere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeBiereRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeBiere::class);
    }

    // /**
    //  * @return TypeBiere[] Returns an array of TypeBiere objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


//    public function findOneById($value): ?TypeBiere
//    {
//        return $this->createQueryBuilder('t')
//                    ->andWhere('t.id = :val')
//                    ->setParameter('val', $value)
//                    ->getQuery()
//                    ->getOneOrNullResult()
//                ;
//    }

//    public function removeOne($id)
//    {
//        dd($id);
//        $this->removeOne($id);
//    }

}

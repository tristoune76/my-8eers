<?php

namespace App\Repository;

use App\Entity\RecipeTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RecipeTemplate|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecipeTemplate|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecipeTemplate[]    findAll()
 * @method RecipeTemplate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecipeTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecipeTemplate::class);
    }

    // /**
    //  * @return RecipeTemplate[] Returns an array of RecipeTemplate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecipeTemplate
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
